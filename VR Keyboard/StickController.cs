using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickController : MonoBehaviour
{
    public GameObject selectedArea;

    private float x_range = 0f;
    private float y_range = 0f;

    void Start()
    {
        var parentRect = transform.parent.GetComponent<RectTransform>().rect;

        x_range = parentRect.width / 2;
        y_range = parentRect.height / 2;

        //x_range *= (2f / 3);
        //y_range *= (3f / 4);

        // 버튼 경계선 +-0.8
        x_range *= (1f / 3) / 0.8f;
        y_range *= (1f / 2) / 0.8f;
    }

    // 원 -> 정사각형 매핑
    // https://stackoverflow.com/questions/13211595/how-can-i-convert-coordinates-on-a-circle-to-coordinates-on-a-square
    private Vector2 CircleToSquare( Vector2 vec2 )
    {
        var u = vec2.x;
        var v = vec2.y;

        return new Vector2(
            Mathf.Sqrt(2 + (u * u) - (v * v) + (2 * u * Mathf.Sqrt(2)))/2
            - Mathf.Sqrt(2 + (u * u) - (v * v) - (2 * u * Mathf.Sqrt(2))) / 2,
            Mathf.Sqrt(2 - (u * u) + (v * v) + (2 * v * Mathf.Sqrt(2))) / 2
            - Mathf.Sqrt(2 - (u * u) + (v * v) - (2 * v * Mathf.Sqrt(2))) / 2
        );
    }

    void Update()
    {
        OVRInput.Update();

        // (X/Y range of -1.0f to 1.0f)
        var leftAxis2D =  OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch);
        var converted = CircleToSquare(leftAxis2D);

        // 조이스틱 좌표 표시용 점 좌표 설정
        var rectTransform = GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(
            converted.x * x_range,
            converted.y * y_range
        );

        // 오른쪽 컨트롤러 버튼 클릭 T/F
        var rightButtonA = OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch);

        // 선택된 영역 표시용 사각형
        var selectedAreaImage = selectedArea.GetComponent<Image>();

        if ( rightButtonA )
        {
            // 누르면 흰색
            selectedAreaImage.color = new Color(1f, 1f, 1f, 0.25f);
        }
        else
        {
            // 떼면 푸른색
            selectedAreaImage.color = new Color(0f, 0.5f, 1f, 0.25f);
        }

        // 선택된 영역 좌표 계산
        var x_index = (int)((leftAxis2D.x + 1) / (2f / 3));
        var y_index = (int)(-(leftAxis2D.y - 1) / (2f / 4));

        var selectedAreaRectTransform = selectedArea.GetComponent<RectTransform>();
        var x = x_index * (selectedAreaRectTransform.rect.width / 3);
        var y = selectedAreaRectTransform.anchoredPosition.y;

        // 정중앙 좌표 무시
        if ( Mathf.Abs(leftAxis2D.y) > 0.01f )
        {
            y = -y_index * (selectedAreaRectTransform.rect.height / 4);
        }

        selectedAreaRectTransform.anchoredPosition = new Vector2(x, y);
    }
}
