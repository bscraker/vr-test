﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTyping : MonoBehaviour
{
    public TMPro.TextMeshPro text;
    public TMPro.TextMeshPro copy;

    private string[] s1 = new string[] {
        "아침 놀 저녁 비요, 저녁 놀 아침 비라.\n\n" +
        "세상의 큰일은 언제나 작은 데서 시작된다.\n\n" +
        "논밭은 잡초 때문에 손해를 보고, 사람은 탐욕 때문에 손해를 본다.",
        "산 사람의 입에 거미줄 치랴?\n\n" +
        "죽이 끓는지 밥이 끓는지 모른다.\n\n" +
        "나와 하늘과 하늘 아래 푸른 산뿐이로다.",
        "어린아이 말도 귀담아들어라.\n\n" +
        "혀는 뼈가 없지만, 뼈를 부러뜨릴 수 있다.\n\n" +
        "말뿐이고 행동이 없는 사람은 잡초가 무성한 정원과 같다.",
        "손가락으로 헤아릴 정도.\n\n" +
        "커도 한 그릇 작아도 한 그릇.\n\n" +
        "한 송이의 국화꽃을 피우기 위해, 봄부터 소쩍새는 그렇게 울었나 보다.",
        "한 손으로는 손뼉을 못 친다.\n\n" +
        "상좌가 많으면 가마솥을 깨뜨린다.\n\n" +
        "부귀한 자리에 있을 때는 빈천한 사람의 고통을 알아야 한다.",
        "어린아이 말도 귀담아들어라.\n\n" +
        "세상의 큰일은 언제나 작은 데서 시작된다.\n\n" +
        "말뿐이고 행동이 없는 사람은 잡초가 무성한 정원과 같다."
    };

    private string[] s2 = new string[] {
        "우리가 정확히 여기 식당 문 앞에서 헤어졌던 게 10시였죠.",
        "적어도 30분은 더 기다려줄 겁니다. 지미가 살아 있다면 녀석은 꼭 그 시간까진 여기 와 줄 겁니다.",
        "15분 사이 몇몇 통행인들이 칼라를 높이 세우고 호주머니에 두 손을 꽂은 채 조용히 왔다 서둘러 사라졌다.",
        "키는 내가 더 컸던 거 같은데, 자넨 그때 이후로도 키가 5에서 8센티미터 더 커졌군.",
        "6.25 전쟁은 1950년 6월 25일 일요일 새벽 4시경 북한군이 북위 38도선 전역에 걸쳐 대한민국을 선전포고 없이 기습 남침하여 발발한 전쟁이다.",
        "2025년 8월 15일은 대한민국 광복 80주년이 되는 날이다."
    };

    private string[] s3 = new string[] {
        "장씨의 주식평가액은 연초보다 1조4천829억원(45.9%) 줄어든 1조7천499억원으로 조사됐다.",
        "관세청은 올해 1~4월 컴퓨터 및 주변기기 수출입액이 작년 동기 대비 40.2% 증가한 133억 달러로 동기 기준 가장 많았다고 31일 밝혔다.",
        "올해 4월까지 수출입 현황을 보면 수출이 70억 달러, 수입이 63억 달러로 작년 동기 대비 각각 65.7%, 19.7% 증가했다.",
    };

    private string[] s4 = new string[] {
        "A barking dog never bites.\n\n" +
        "A bad workman always blames his tools.\n\n" +
        "A feather in hand is better than a bird in the air.",
        "A big fish in a little pond.\n\n" +
        "A bird in the hand is worth two in the bush.\n\n" +
        "A history is always written by the winning side.",
        "Out of sight, out of mind.\n\n" +
        "If you run after two hares, you will catch neither.\n\n" +
        "A journey of a thousand miles begins with a single step.",
        "A big fish in a little pond.\n\n" +
        "A history is always written by the winning side.\n\n" +
        "A feather in hand is better than a bird in the air.",
        "A barking dog never bites.\n\n" +
        "A bad workman always blames his tools.\n\n" +
        "If you run after two hares, you will catch neither.",
        "Out of sight, out of mind.\n\n" +
        "Nothing ventured, nothing gained.\n\n" +
        "A journey of a thousand miles begins with a single step."
    };

    private string[] s5 = new string[] {
        "The shiny receiver hung on the side of the box. I even remembered the number, 105.",
        "This admixture of modern human and Neanderthal genes is estimated to have occurred roughly between 50,000 and 60,000 years ago in Southern Europe.",
        "I had about half an hour between plan connections, and I spent 15 minutes or so on the phone with my sister who lived there now, happily mellowed by marriage and motherhood.",
        "Independence Day is a federal holiday in the United States commemorating the Declaration of Independence of the United States, which was ratified by the Second Continental Congress on July 4, 1776.",
        "Reprinted with permission in the December 1999 issue of the Singing Wires newsletter, TCI club.",
        "This admixture of modern human and Neanderthal genes is estimated to have occurred roughly between 50,000 and 60,000 years ago in Southern Europe."
    };

    private string[] s6 = new string[] {
        "In terms of hospital readmissions about 9% of 106,000 individuals had to return for hospital treatment within two months of discharge.",
        "Some early studies suggest 10% to 20% of people with COVID‑19 will experience symptoms lasting longer than a month.",
        "Like many stablecoins, UST was pegged at a 1-to-1 ratio with the dollar.",
    };

    private string[] s7 = new string[] {
        "러시아 관영 타스통신과 극동 매체 등에 따르면 올해 1~5월 북극해 항로(NSR-Northern Sea Route)를 통한 해상 운송량은 1천300만t으로 집계됐다.",
        "러시아의 북극해 항로는 북극권 카르스키예 해협(Kara Strait)에서 추코트카 자치구의 프로비데니야만(Providence Bay)까지 약 5천600km에 이른다.",
        "2021년 발표된 논문에 따르면, Vision Transformer(ViT)는 기존의 SotA CNN들에 비해 요구되는 계산량은 상당히 적으면서도 성능은 좋았다고 한다.",
        "2020년 Facebook 팀은 네트워크 구조가 단순하면서 end-to-end 학습이 가능한 DETR(End-to-end object detection with transformers)을 공개했다.",
    };

    private string[][] ss;

    private void PrintRandomTyping( int index )
    {
        var s_ = ss[index];
        var s = s_[Random.Range(0, s_.Length)];

        text.text = s;
        copy.text = s;
    }

    public void PrintRandomTyping_1( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(0);
    }

    public void PrintRandomTyping_2( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(1);
    }

    public void PrintRandomTyping_3( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(2);
    }

    public void PrintRandomTyping_4( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(3);
    }

    public void PrintRandomTyping_5( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(4);
    }

    public void PrintRandomTyping_6( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(5);
    }

    public void PrintRandomTyping_7( bool b )
    {
        if ( !b )
        {
            return;
        }

        PrintRandomTyping(6);
    }

    void Start()
    {
        ss = new string[][] {
            s1, s2, s3, s4, s5, s6, s7
        };
    }
}
