using Oculus.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prototype_1 : MonoBehaviour
{
    public Cheonjiin cheonjiin;
    public pt1_Special special;
    public pt1_Korean number;
    public pt1_English english;
    public pt1_Korean korean;

    public GameObject keyboard_0;
    public GameObject keyboard_1;
    public GameObject keyboard_2;
    public GameObject keyboard_3;

    public MeshRenderer left_mesh;

    public Material left_0;
    public Material left_1;
    public Material left_2;
    public Material left_3;

    public MeshRenderer temp;

    public Material temp_0;
    public Material temp_1;

    public GameObject temp_overlay;

    public MeshRenderer english_mesh;

    public Material english_low;
    public Material english_up;

    public MeshRenderer special_mesh;

    public Material special_0;
    public Material special_1;
    public Material special_2;

    public GameObject shift;
    public GameObject shift_overlay;

    public MeshRenderer shift_mesh;

    public Material shift_0;
    public Material shift_1;

    public AudioTrigger sound_0;
    public AudioTrigger sound_1;

    private int currentKeyboard = 3;

    private bool leftTrigger = false;
    private bool leftGrab = false;
    private bool rightTrigger = false;
    private bool rightGrab = false;

    private bool leftX = false;
    private bool leftY = false;
    private bool rightA = false;
    private bool rightB = false;

    private bool rightStick = false;

    private GameObject[] keyboards;
    private Material[] ms;

    private delegate void Func();
    private Func[] func;

    private int special_page = 0;

    void Start()
    {
        keyboards = new GameObject[] {
            keyboard_0,
            keyboard_1,
            keyboard_2,
            keyboard_3
        };

        ms = new Material[] {
            left_0,
            left_1,
            left_2,
            left_3
        };

        func = new Func[] {
            cheonjiin.PressKeypad0,
            cheonjiin.PressKeypad7,
            cheonjiin.PressKeypad8,
            cheonjiin.PressKeypad9,
            cheonjiin.PressKeypad4,
            cheonjiin.PressKeypad5,
            cheonjiin.PressKeypad6,
            cheonjiin.PressKeypad1,
            cheonjiin.PressKeypad2,
            cheonjiin.PressKeypad3
        };
    }

    void Update()
    {
        OVRInput.Update();

        var leftButtonX = OVRInput.Get(OVRInput.Button.Three);
        var leftButtonY = OVRInput.Get(OVRInput.Button.Four);
        var rightButtonA = OVRInput.Get(OVRInput.Button.One);
        var rightButtonB = OVRInput.Get(OVRInput.Button.Two);

        var leftIndexTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger);
        var leftHandTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger);
        var rightIndexTrigger = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger);
        var rightHandTrigger = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger);

        var rightAxis2D = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        if ( leftIndexTrigger > 0.75f )
        {
            if ( !leftTrigger )
            {
                if ( currentKeyboard == 1 )
                {
                    cheonjiin.PressKeypadMinus2();
                }
                else
                {
                    cheonjiin.PressKeypadMinus();
                }

                temp_overlay.SetActive(true);

                sound_0.PlayAudio();

                leftTrigger = true;
            }
        }
        else if ( leftIndexTrigger < 0.25f )
        {
            if ( leftTrigger )
            {
                temp_overlay.SetActive(false);

                sound_1.PlayAudio();

                leftTrigger = false;
            }
        }
        
        if ( leftHandTrigger > 0.75f )
        {
            if ( !leftGrab )
            {
                if ( currentKeyboard == 2 )
                {
                    cheonjiin.PressShift();
                }

                sound_0.PlayAudio();

                leftGrab = true;
            }
        }
        else if ( leftHandTrigger < 0.25f )
        {
            if ( leftGrab )
            {
                sound_1.PlayAudio();

                leftGrab = false;
            }
        }

        if ( currentKeyboard == 0 )
        {
            if ( rightAxis2D.x > 0.75f )
            {
                if ( !rightStick )
                {
                    special_page += 1;
                    
                    sound_0.PlayAudio();

                    rightStick = true;
                }
            }
            else if ( rightAxis2D.x < -0.75f )
            {
                if ( !rightStick )
                {
                    special_page += 2;

                    sound_0.PlayAudio();

                    rightStick = true;
                }
            }
            else
            {
                if ( rightStick )
                {
                    sound_1.PlayAudio();

                    rightStick = false;
                }
            }

            special_page %= 3;

            if ( special_page == 0 )
            {
                special_mesh.material = special_0;
            }
            else if ( special_page == 1 )
            {
                special_mesh.material = special_1;
            }
            else if ( special_page == 2 )
            {
                special_mesh.material = special_2;
            }
        }
        else if ( currentKeyboard == 2 )
        {
            if ( cheonjiin.shift == 0 )
            {
                english_mesh.material = english_low;

                shift_mesh.material = shift_0;

                shift_overlay.SetActive(false);
            }
            else
            {
                english_mesh.material = english_up;

                if ( cheonjiin.shift == 1 )
                {
                    shift_mesh.material = shift_0;
                }
                else
                {
                    shift_mesh.material = shift_1;
                }

                shift_overlay.SetActive(true);
            }
        }

        if ( rightIndexTrigger > 0.75f )
        {
            if ( !rightTrigger )
            {
                cheonjiin.PressSpace();

                sound_0.PlayAudio();

                rightTrigger = true;
            }
        }
        else if ( rightIndexTrigger < 0.25f )
        {
            if ( rightTrigger )
            {
                sound_1.PlayAudio();

                rightTrigger = false;
            }
        }

        if ( rightHandTrigger > 0.75f )
        {
            if ( !rightGrab )
            {
                cheonjiin.PressEnter();

                sound_0.PlayAudio();

                rightGrab = true ;
            }
        }
        else if ( rightHandTrigger < 0.25f )
        {
            if ( rightGrab )
            {
                sound_1.PlayAudio();

                rightGrab = false;
            }
        }

        if ( rightButtonA )
        {
            if ( !rightA )
            {
                if ( currentKeyboard == 0 )
                {
                    var index = special.GetCurrentIndex();
                
                    if ( index == 0 )
                    {
                        index = 10;
                    }

                    index -= 1;

                    index += (special_page * 9);

                    cheonjiin.PressSpecial(index);
                }
                else if ( currentKeyboard == 1 )
                {
                    cheonjiin.SendText(number.GetCurrentIndex().ToString());
                }
                else if ( currentKeyboard == 2 )
                {
                    cheonjiin.PressEnglish(english.GetCurrentIndex() - 2);
                }
                else if ( currentKeyboard == 3 )
                {
                    func[korean.GetCurrentIndex()]();
                }

                sound_0.PlayAudio();

                rightA = true;
            }
        }
        else
        {
            if ( rightA )
            {
                sound_1.PlayAudio();

                rightA = false;
            }
        }

        if ( rightButtonB )
        {
            if ( !rightB )
            {
                cheonjiin.PressBackSpace();

                sound_0.PlayAudio();

                rightB = true;
            }
        }
        else
        {
            if ( rightB )
            {
                sound_1.PlayAudio();

                rightB = false;
            }
        }

        var keyboard_index = currentKeyboard;

        if ( leftButtonX )
        {
            if ( !leftX )
            {
                keyboard_index += 1;

                sound_0.PlayAudio();

                leftX = true;
            }
        }
        else
        {
            if ( leftX )
            {
                sound_1.PlayAudio();

                leftX = false;
            }
        }
        
        if ( leftButtonY )
        {
            if ( !leftY )
            {
                keyboard_index += 3;
                
                sound_0.PlayAudio();

                leftY = true;
            }
        }
        else
        {
            if ( leftY )
            {
                sound_1.PlayAudio();

                leftY = false;
            }
        }

        keyboard_index %= 4;

        if ( keyboard_index != currentKeyboard )
        {
            if ( currentKeyboard == 2 )
            {
                shift.SetActive(false);
            }

            currentKeyboard = keyboard_index;

            foreach ( var item in keyboards )
            {
                item.SetActive(false);
            }

            keyboards[currentKeyboard].SetActive(true);
            left_mesh.material = ms[currentKeyboard];

            cheonjiin.ClearBuffer();

            if ( currentKeyboard == 2 )
            {
                cheonjiin.InitializeShift();

                shift.SetActive(true);
                shift_overlay.SetActive(false);
            }

            if ( currentKeyboard == 1 )
            {
                temp.material = temp_1;
            }
            else
            {
                temp.material = temp_0;
            }

            if ( currentKeyboard == 0 )
            {
                special_page = 0;
            }
        }
    }

    public void Initialize()
    {
        Start();

        currentKeyboard = 3;

        shift.SetActive(false);

        foreach ( var item in keyboards )
        {
            item.SetActive(false);
        }

        keyboards[currentKeyboard].SetActive(true);
        left_mesh.material = ms[currentKeyboard];

        cheonjiin.ClearBuffer();
    }
}
