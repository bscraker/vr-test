using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pt1_Parent : MonoBehaviour
{
    public GameObject leftController;
    public GameObject backup;

    void Start()
    {
        transform.rotation = backup.transform.rotation;
        transform.position = leftController.transform.position;
        transform.SetParent(leftController.transform);
    }
}
