using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCursor : MonoBehaviour
{
    public Transform point;

    public float distance = 0f;
    public float angle = 0f;

    void Update()
    {
        OVRInput.Update();

        var leftAxis2D = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch);

        distance = Vector2.Distance(Vector2.zero, leftAxis2D);
        angle = Vector2.SignedAngle(new Vector2(0f, -1f), leftAxis2D) + 180f;

        MovePoint(leftAxis2D);
    }

    private void MovePoint( Vector2 leftAxis2D )
    {
        var point_ = new Vector3(-leftAxis2D.x, 0f, -leftAxis2D.y);

        var r = 10f;

        if ( distance >= (1 / 3f) )
        {
            r *= ((1 / 3f) / distance) + ((distance - (1 / 3f)) / distance / 10f);
        }

        point.localPosition = point_ * r;
    }
}
