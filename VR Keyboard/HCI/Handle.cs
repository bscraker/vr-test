using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handle : MonoBehaviour
{
    public GameObject left;
    public GameObject right;

    public Material idle;
    public Material hover;
    public Material grab;

    private bool onLeft = false;
    private bool onRight = false;

    private int onGrab = 0;

    void Update()
    {
        OVRInput.Update();

        var leftIndexTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.Touch);
        var leftHandTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.Touch);

        var rightIndexTrigger = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger, OVRInput.Controller.Touch);
        var rightHandTrigger = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch);

        if ( onLeft || onRight )
        {
            GetComponent<MeshRenderer>().material = hover;
        }
        else
        {
            GetComponent<MeshRenderer>().material = idle;
        }

        if ( (leftIndexTrigger > 0.75f) || (leftHandTrigger > 0.75f) )
        {
            if ( onLeft && (onGrab != 2) )
            {
                if ( onGrab == 0 )
                {
                    transform.SetParent(left.transform);
                    GetComponent<Rigidbody>().isKinematic = true;
                }

                GetComponent<MeshRenderer>().material = grab;

                onGrab = 1;
            }
        }
        else if ( onGrab == 1 )
        {
            transform.SetParent(null);
            GetComponent<Rigidbody>().isKinematic = false;

            onGrab = 0;
        }
        
        if ( (rightIndexTrigger > 0.75f) || (rightHandTrigger > 0.75f) )
        {
            if ( onRight && (onGrab != 1) )
            {
                if ( onGrab == 0 )
                {
                    transform.SetParent(right.transform);
                    GetComponent<Rigidbody>().isKinematic = true;
                }

                GetComponent<MeshRenderer>().material = grab;

                onGrab = 2;
            }
        }
        else if ( onGrab == 2 )
        {
            transform.SetParent(null);
            GetComponent<Rigidbody>().isKinematic = false;

            onGrab = 0;
        }
    }

    private void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject == left )
        {
            onLeft = true;
        }
        else if ( other.gameObject == right )
        {
            onRight = true;
        }
    }

    private void OnTriggerExit( Collider other )
    {
        if ( other.gameObject == left )
        {
            onLeft = false;
        }
        else if ( other.gameObject == right )
        {
            onRight = false;
        }
    }
}
