using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pt1_English : MonoBehaviour
{
    public PointCursor point;

    public MeshRenderer overlay;

    public Material key2;
    public Material key2_;
    public Material key3;
    public Material key3_;
    public Material key4;
    public Material key4_;
    public Material key5;
    public Material key5_;
    public Material key6;
    public Material key6_;
    public Material key7;
    public Material key7_;
    public Material key8;
    public Material key8_;
    public Material key9;
    public Material key9_;

    private Material[] m;

    void Start()
    {
        m = new Material[] {
            key2, key2_,
            key3, key3_,
            key4, key4_,
            key5, key5_,
            key6, key6_,
            key7, key7_,
            key8, key8_,
            key9, key9_
        };
    }

    void Update()
    {
        OVRInput.Update();

        int index = GetCurrentIndex();

        index -= 2;
        index *= 2;

        var rightButtonA = OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch);

        if ( rightButtonA )
        {
            index += 1;
        }

        ChangeMaterial(index);
    }

    public int GetCurrentIndex()
    {
        var d = point.distance;
        var a = point.angle;

        var index = 5;

        if ( d >= (1 / 3f) )
        {
            var angle = a / (360 / 7f);
            angle = (int)angle;

            if ( (angle == 0) )
            {
                index = 2;
            }
            else if ( angle == 1 )
            {
                index = 4;
            }
            else if ( angle == 2 )
            {
                index = 7;
            }
            else if ( angle == 3 )
            {
                index = 8;
            }
            else if ( angle == 4 )
            {
                index = 9;
            }
            else if ( angle == 5 )
            {
                index = 6;
            }
            else if ( angle == 6 )
            {
                index = 3;
            }
        }

        return index;
    }

    private void ChangeMaterial( int index )
    {
        overlay.material = m[index];
    }
}
