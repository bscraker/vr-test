using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMask : MonoBehaviour
{
    void Start()
    {
        var _textMeshPro = GetComponent<TMPro.TextMeshPro>();

        var material = new Material(_textMeshPro.fontMaterial);
        // material.SetFloat("_StencilID", 1.0f);
        material.SetFloat("_StencilComp", 3.0f);

        _textMeshPro.fontMaterial = material;
    }
}
