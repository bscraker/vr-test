﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using TMPro;

public class Cheonjiin : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro txt;

    public TextMeshPro shiftText;

    public TextMeshPro english_0;
    public TextMeshPro english_1;
    public TextMeshPro english_2;
    public TextMeshPro english_3;
    public TextMeshPro english_4;
    public TextMeshPro english_5;
    public TextMeshPro english_6;
    public TextMeshPro english_7;

    private const short CHO_SUNG = 0;
    private const short JUNG_SUNG = 1;
    private const short JONG_SUNG = 2;

    private char tuk = ' ';
    private char[] buffer = { ' ', ' ', ' ' };

    private string special = " ";
    private string alphabet = " ";
    public int shift = 0;

    private string dummyText = "";

    private float time = 0.0f;

    public void Initialize()
    {
        ClearBuffer();

        dummyText = "";

        time = 0.0f;
    }

    public void InitializeShift()
    {
        if ( shift == 1 )
        {
            PressShift();
            PressShift();
        }
        else if ( shift == 2 )
        {
            PressShift();
        }
    }

    public void ClearBuffer()
    {
        if ( (buffer[0] != ' ') && ("ㆍᆢ".Contains(buffer[1])) && (buffer[2] == ' ') )
        {
            SendText(buffer[1].ToString());
        }

        buffer[CHO_SUNG] = ' ';
        buffer[JUNG_SUNG] = ' ';
        buffer[JONG_SUNG] = ' ';
        tuk = ' ';

        special = " ";
        alphabet = " ";
    }

    private static bool GBCCheck(char Jong, ref char F, ref char L)
    {
        bool result = true;
        switch (Jong)
        {
            case 'ㄳ':
                F = 'ㄱ';
                L = 'ㅅ';
                break;
            case 'ㄵ':
                F = 'ㄴ';
                L = 'ㅈ';
                break;
            case 'ㄶ':
                F = 'ㄴ';
                L = 'ㅎ';
                break;
            case 'ㄺ':
                F = 'ㄹ';
                L = 'ㄱ';
                break;
            case 'ㄻ':
                F = 'ㄹ';
                L = 'ㅁ';
                break;
            case 'ㄼ':
                F = 'ㄹ';
                L = 'ㅂ';
                break;
            case 'ㄽ':
                F = 'ㄹ';
                L = 'ㅅ';
                break;
            case 'ㄾ':
                F = 'ㄹ';
                L = 'ㅌ';
                break;
            case 'ㄿ':
                F = 'ㄹ';
                L = 'ㅍ';
                break;
            case 'ㅀ':
                F = 'ㄹ';
                L = 'ㅎ';
                break;
            case 'ㅄ':
                F = 'ㅂ';
                L = 'ㅅ';
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    private void SendBuffer()
    {
        SendBackSpace();

        var text = Hap(buffer[0], buffer[1], buffer[2]);

        if ( text.Length > 1 )
        {
            buffer[0] = buffer[2];
            buffer[1] = ' ';
            buffer[2] = ' ';
        }

        SendText(text);
    }

    public void SendText( string text )
    {
        dummyText += text;
    }

    private static string Hap(char ch1, char ch2, char ch3)
    {
        
        string choSung = "ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ";
        string jungSung = "ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ";
        string jongSung = " ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ";

        int choindex = choSung.IndexOf(ch1);
        int jungindex = jungSung.IndexOf(ch2);
        int jongindex = jongSung.IndexOf(ch3);

        bool isValidIndex = (-1 != choindex) && (-1 != jungindex) && (-1 != jongindex);
        if (!isValidIndex)
        {
            return string.Format("{0}{1}{2}", ch1.ToString().Replace(" ", ""), ch2.ToString().Replace(" ", ""), ch3.ToString().Replace(" ", ""));
        }

        int uniValue = (choindex * 21 * 28) + (jungindex * 28) + (jongindex) + 0xAC00;
        return string.Format("{0}", (char)uniValue);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) // 백스페이스
        {
            PressBackSpace();
        }
        else if (Input.GetKeyDown(KeyCode.KeypadPeriod)) // 띄어쓰기
        {
            PressSpace();
        }
        else if (Input.GetKeyDown(KeyCode.KeypadEnter)) // 줄바꿈
        {
            PressEnter();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            PressKeypad1();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            PressKeypad2();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            PressKeypad3();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            PressKeypad4();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            PressKeypad5();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            PressKeypad6();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            PressKeypad7();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            PressKeypad8();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            PressKeypad9();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            PressKeypad0();
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus)) // .,?!
        {
            PressKeypadMinus();
        }
        

        string temp = " ";

        time += Time.deltaTime;
        
        if ( (int)(time / 0.333f) % 2 == 0 )
        {
            temp = "<color=black>|</color>" + temp;
        }
        else
        {
            temp = "<color=white>|</color>" + temp;
        }

        if ( (buffer[0] != ' ') && ("ㆍᆢ".Contains(buffer[1])) && (buffer[2] == ' ') )
        {
            temp = buffer[1] + temp;
        }

        txt.SetText(dummyText + temp);
    }
    
    public void PressKeypadMinus()
    {
        buffer[CHO_SUNG] = ' ';
        buffer[JUNG_SUNG] = ' ';
        buffer[JONG_SUNG] = ' ';
        
        bool noChangeChar = false;

        switch (tuk)
        {
            case ' ':
                tuk = '.';
                noChangeChar = true;
                break;
            case '.':
                tuk = ',';
                break;
            case ',':
                tuk = '?';
                break;
            case '?':
                tuk = '!';
                break;
            case '!':
                tuk = '.';
                break;
            default:
                noChangeChar = true;
                break;

        }

        if (!noChangeChar)
        {
            SendBackSpace();
        }

        SendText(tuk.ToString());
    }

    public void PressKeypadMinus2()
    {
        buffer[CHO_SUNG] = ' ';
        buffer[JUNG_SUNG] = ' ';
        buffer[JONG_SUNG] = ' ';
        
        bool noChangeChar = false;

        switch (tuk)
        {
            case ' ':
                tuk = '.';
                noChangeChar = true;
                break;
            case '.':
                tuk = ',';
                break;
            case ',':
                tuk = '-';
                break;
            case '-':
                tuk = '/';
                break;
            case '/':
                tuk = '.';
                break;
            default:
                noChangeChar = true;
                break;

        }

        if (!noChangeChar)
        {
            SendBackSpace();
        }

        SendText(tuk.ToString());
    }

    public void PressKeypad0()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;

            switch (buffer[CHO_SUNG])
            {
                case 'ㅇ':
                    buffer[CHO_SUNG] = 'ㅁ';
                    break;

                case 'ㅁ':
                    buffer[CHO_SUNG] = 'ㅇ';
                    break;

                default:
                    buffer[CHO_SUNG] = 'ㅇ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;

            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㅁ':
                    buffer[JONG_SUNG] = 'ㅇ';
                    break;
                case 'ㅇ':
                    buffer[JONG_SUNG] = 'ㅁ';
                    break;
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄻ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅇ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㅇ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad9()
    {
        tuk = ' ';

        if (buffer[JONG_SUNG] == ' ')
        {
            if (buffer[CHO_SUNG] == ' ' && buffer[JUNG_SUNG] == ' ')
            {
                buffer[JUNG_SUNG] = 'ㅡ';
                SendText("ㅡ");
            }
            else
            {
                bool noBufferSend = false;
                switch (buffer[JUNG_SUNG])
                {
                    case 'ㆍ':
                        buffer[JUNG_SUNG] = 'ㅗ';
                        break;

                    case 'ᆢ':
                        buffer[JUNG_SUNG] = 'ㅛ';
                        break;

                    case ' ':
                        buffer[JUNG_SUNG] = 'ㅡ';
                        break;

                    case 'ㅡ':
                    default:
                        if (buffer[CHO_SUNG] == ' ')
                        {
                            buffer[JONG_SUNG] = ' ';
                        }
                        buffer[CHO_SUNG] = ' ';
                        buffer[JUNG_SUNG] = 'ㅡ';

                        SendText((buffer[CHO_SUNG] == ' ' ? "" : buffer[CHO_SUNG].ToString()));
                        SendText("ㅡ");
                        noBufferSend = true;
                        break;

                }
                if (!noBufferSend)
                {
                    SendBuffer();
                }
            }
        }
        else if (buffer[JONG_SUNG] != ' ')
        {
            char firstJong = new char();
            char secondJong = new char();

            SendBackSpace();

            if (GBCCheck(buffer[JONG_SUNG], ref firstJong, ref secondJong))
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], firstJong));
                buffer[JUNG_SUNG] = 'ㅡ';
                buffer[CHO_SUNG] = secondJong;
                buffer[JONG_SUNG] = ' ';
            }
            else
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], ' '));
                buffer[JUNG_SUNG] = 'ㅡ';
                buffer[CHO_SUNG] = buffer[JONG_SUNG];
                buffer[JONG_SUNG] = ' ';
            }

            SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], buffer[JONG_SUNG]));
        }
    }

    public void PressKeypad8()
    {
        tuk = ' ';

        if (buffer[JONG_SUNG] == ' ')
        {
            if (buffer[CHO_SUNG] == ' ' && buffer[JUNG_SUNG] == ' ')
            {
                buffer[JUNG_SUNG] = 'ㆍ';
                SendText("ㆍ");
            }
            else
            {
                bool noBufferSend = false;

                switch (buffer[JUNG_SUNG])
                {
                    case ' ':
                        buffer[JUNG_SUNG] = 'ㆍ';
                        noBufferSend = (buffer[CHO_SUNG] != ' ');
                        break;
                    case 'ㆍ':
                        buffer[JUNG_SUNG] = 'ᆢ';
                        noBufferSend = (buffer[CHO_SUNG] != ' ');
                        break;
                    case 'ᆢ':
                        buffer[JUNG_SUNG] = 'ㆍ';
                        noBufferSend = (buffer[CHO_SUNG] != ' ');
                        break;
                    case 'ㅣ':
                        buffer[JUNG_SUNG] = 'ㅏ';
                        break;
                    case 'ㅏ':
                        buffer[JUNG_SUNG] = 'ㅑ';
                        break;
                    case 'ㅑ':
                        buffer[JUNG_SUNG] = 'ㅏ';
                        break;
                    case 'ㅡ':
                        buffer[JUNG_SUNG] = 'ㅜ';
                        break;
                    case 'ㅜ':
                        buffer[JUNG_SUNG] = 'ㅠ';
                        break;
                    case 'ㅠ':
                        buffer[JUNG_SUNG] = 'ㅜ';
                        break;
                    case 'ㅚ':
                        buffer[JUNG_SUNG] = 'ㅘ';
                        break;
                    case 'ㅟ':
                        buffer[JUNG_SUNG] = 'ㅝ';
                        break;
                    default:
                        if (buffer[CHO_SUNG] == ' ')
                        {
                            buffer[JONG_SUNG] = ' ';
                        }
                        buffer[CHO_SUNG] = ' ';
                        buffer[JUNG_SUNG] = 'ㆍ';

                        SendText((buffer[CHO_SUNG] == ' ' ? "" : buffer[CHO_SUNG].ToString()));
                        SendText("ㆍ");
                        noBufferSend = true;
                        break;
                }

                if (!noBufferSend)
                {
                    SendBuffer();
                }
            }
        }
        else if (buffer[JONG_SUNG] != ' ')
        {
            char firstJong = new char();
            char secondJong = new char();

            SendBackSpace();

            if (GBCCheck(buffer[JONG_SUNG], ref firstJong, ref secondJong))
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], firstJong));
                buffer[JUNG_SUNG] = 'ㆍ';
                buffer[CHO_SUNG] = secondJong;
                buffer[JONG_SUNG] = ' ';
            }
            else
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], ' '));
                buffer[JUNG_SUNG] = 'ㆍ';
                buffer[CHO_SUNG] = buffer[JONG_SUNG];
                buffer[JONG_SUNG] = ' ';
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
    }

    public void PressKeypad7()
    {
        tuk = ' ';

        if (buffer[JONG_SUNG] == ' ')
        {
            if (buffer[CHO_SUNG] == ' ' && buffer[JUNG_SUNG] == ' ')
            {
                buffer[JUNG_SUNG] = 'ㅣ';
                SendText("ㅣ");
            }
            else
            {
                bool noBufferSend = false;
                switch (buffer[JUNG_SUNG])
                {
                    case ' ':
                        buffer[JUNG_SUNG] = 'ㅣ';
                        break;
                    case 'ㅓ':
                        buffer[JUNG_SUNG] = 'ㅔ';
                        break;
                    case 'ㆍ':
                        buffer[JUNG_SUNG] = 'ㅓ';
                        break;
                    case 'ᆢ':
                        buffer[JUNG_SUNG] = 'ㅕ';
                        break;
                    case 'ㅡ':
                        buffer[JUNG_SUNG] = 'ㅢ';
                        break;
                    case 'ㅗ':
                        buffer[JUNG_SUNG] = 'ㅚ';
                        break;
                    case 'ㅜ':
                        buffer[JUNG_SUNG] = 'ㅟ';
                        break;
                    case 'ㅏ':
                        buffer[JUNG_SUNG] = 'ㅐ';
                        break;
                    case 'ㅑ':
                        buffer[JUNG_SUNG] = 'ㅒ';
                        break;
                    case 'ㅕ':
                        buffer[JUNG_SUNG] = 'ㅖ';
                        break;
                    case 'ㅠ':
                        buffer[JUNG_SUNG] = 'ㅝ';
                        break;
                    case 'ㅝ':
                        buffer[JUNG_SUNG] = 'ㅞ';
                        break;
                    case 'ㅘ':
                        buffer[JUNG_SUNG] = 'ㅙ';
                        break;
                    case 'l':
                    default:
                        if (buffer[CHO_SUNG] == ' ')
                        {
                            buffer[JONG_SUNG] = ' ';
                        }
                        buffer[CHO_SUNG] = ' ';
                        buffer[JUNG_SUNG] = 'ㅣ';
                        noBufferSend = true;
                        SendText((buffer[CHO_SUNG] == ' ' ? "" : buffer[CHO_SUNG].ToString()));
                        SendText("ㅣ");
                        break;
                }
                if (!noBufferSend)
                {
                    SendBuffer();
                }
            }
        }
        else if (buffer[JONG_SUNG] != ' ')
        {
            char firstJong = new char();
            char secondJong = new char();

            SendBackSpace();

            if (GBCCheck(buffer[JONG_SUNG], ref firstJong, ref secondJong))
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], firstJong));

                buffer[JUNG_SUNG] = 'ㅣ';
                buffer[CHO_SUNG] = secondJong;
                buffer[JONG_SUNG] = ' ';
            }
            else
            {
                SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], ' '));

                buffer[JUNG_SUNG] = 'ㅣ';
                buffer[CHO_SUNG] = buffer[JONG_SUNG];
                buffer[JONG_SUNG] = ' ';
            }

            SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], buffer[JONG_SUNG]));
        }
    }

    public void PressKeypad6()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;

            switch (buffer[CHO_SUNG])
            {
                case 'ㄷ':
                    buffer[CHO_SUNG] = 'ㅌ';
                    break;
                case 'ㅌ':
                    buffer[CHO_SUNG] = 'ㄸ';
                    break;
                case 'ㄸ':
                    buffer[CHO_SUNG] = 'ㄷ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄷ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㅌ':
                    buffer[JONG_SUNG] = 'ㄷ';
                    break;
                case 'ㄷ':
                    buffer[JONG_SUNG] = 'ㅌ';
                    break;
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄾ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄷ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㄷ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad5()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;

            switch (buffer[CHO_SUNG])
            {
                case 'ㄴ':
                    buffer[CHO_SUNG] = 'ㄹ';
                    break;
                case 'ㄹ':
                    buffer[CHO_SUNG] = 'ㄴ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄴ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄴ';
                    break;
                case 'ㄴ':
                    buffer[JONG_SUNG] = 'ㄹ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄴ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㄴ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad4()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;
            switch (buffer[CHO_SUNG])
            {
                case 'ㄱ':
                    buffer[CHO_SUNG] = 'ㅋ';
                    break;
                case 'ㅋ':
                    buffer[CHO_SUNG] = 'ㄲ';
                    break;
                case 'ㄲ':
                    buffer[CHO_SUNG] = 'ㄱ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄱ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㄲ':
                    buffer[JONG_SUNG] = 'ㄱ';
                    break;
                case 'ㄱ':
                    buffer[JONG_SUNG] = 'ㅋ';
                    break;
                case 'ㅋ':
                    buffer[JONG_SUNG] = 'ㄲ';
                    break;
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄺ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㄱ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㄱ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad3()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;
            switch (buffer[CHO_SUNG])
            {
                case 'ㅈ':
                    buffer[CHO_SUNG] = 'ㅊ';
                    break;
                case 'ㅊ':
                    buffer[CHO_SUNG] = 'ㅉ';
                    break;
                case 'ㅉ':
                    buffer[CHO_SUNG] = 'ㅈ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅈ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㅊ':
                    buffer[JONG_SUNG] = 'ㅈ';
                    break;
                case 'ㅈ':
                    buffer[JONG_SUNG] = 'ㅊ';
                    break;
                case 'ㄴ':
                    buffer[JONG_SUNG] = 'ㄵ';
                    break;
                case 'ㄵ':
                    buffer[JONG_SUNG] = 'ㄴ';
                    SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], buffer[JONG_SUNG]));
                    buffer[CHO_SUNG] = 'ㅉ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendBackSpace();
                    SendText("ㅉ");
                    noBufferSend = true;
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅈ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㅈ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad2()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;
            switch (buffer[CHO_SUNG])
            {
                case 'ㅅ':
                    buffer[CHO_SUNG] = 'ㅎ';
                    break;
                case 'ㅎ':
                    buffer[CHO_SUNG] = 'ㅆ';
                    break;
                case 'ㅆ':
                    buffer[CHO_SUNG] = 'ㅅ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅅ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool noBufferSend = false;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㅆ':
                    buffer[JONG_SUNG] = 'ㅅ';
                    break;
                case 'ㅅ':
                    buffer[JONG_SUNG] = 'ㅎ';
                    break;
                case 'ㅎ':
                    buffer[JONG_SUNG] = 'ㅆ';
                    break;
                case 'ㄴ':
                    buffer[JONG_SUNG] = 'ㄶ';
                    break;
                case 'ㄱ':
                    buffer[JONG_SUNG] = 'ㄳ';
                    break;
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄽ';
                    break;
                case 'ㄽ':
                    buffer[JONG_SUNG] = 'ㅀ';
                    break;
                case 'ㅀ':
                    buffer[JONG_SUNG] = 'ㄽ';
                    break;
                case 'ㅂ':
                    buffer[JONG_SUNG] = 'ㅄ';
                    break;
                case 'ㅄ':
                    buffer[JONG_SUNG] = 'ㅂ';
                    SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], buffer[JONG_SUNG]));
                    buffer[CHO_SUNG] = 'ㅆ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendBackSpace();
                    SendText("ㅆ");
                    noBufferSend = true;
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅅ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㅅ");
                    noBufferSend = true;
                    break;
            }
            if (!noBufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressKeypad1()
    {
        tuk = ' ';

        if (buffer[JUNG_SUNG] == ' ')
        {
            bool noChangeChar = false;

            switch (buffer[CHO_SUNG])
            {
                case 'ㅂ':
                    buffer[CHO_SUNG] = 'ㅍ';
                    break;
                case 'ㅍ':
                    buffer[CHO_SUNG] = 'ㅃ';
                    break;
                case 'ㅃ':
                    buffer[CHO_SUNG] = 'ㅂ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅂ';
                    noChangeChar = true;
                    break;
            }

            if (!noChangeChar)
            {
                SendBackSpace();
            }

            SendText(buffer[CHO_SUNG].ToString());
        }
        else
        {
            bool BufferSend = true;
            switch (buffer[JONG_SUNG])
            {
                case ' ':
                case 'ㅍ':
                    buffer[JONG_SUNG] = 'ㅂ';
                    break;
                case 'ㅂ':
                    buffer[JONG_SUNG] = 'ㅍ';
                    break;
                case 'ㄹ':
                    buffer[JONG_SUNG] = 'ㄼ';
                    break;
                case 'ㄼ':
                    buffer[JONG_SUNG] = 'ㄿ';
                    break;
                case 'ㄿ':
                    buffer[JONG_SUNG] = 'ㄼ';
                    break;
                default:
                    buffer[CHO_SUNG] = 'ㅂ';
                    buffer[JUNG_SUNG] = ' ';
                    buffer[JONG_SUNG] = ' ';
                    SendText("ㅂ");
                    BufferSend = false;
                    break;
            }
            if (BufferSend)
            {
                SendBuffer();
            }
        }
    }

    public void PressEnter()
    {
        ClearBuffer();

        SendText("\n");

        if ( shift == 1 )
        {
            PressShift();
            PressShift();
        }
    }

    public void PressSpace()
    {
        if ((buffer[CHO_SUNG] != ' ') || (buffer[JUNG_SUNG] != ' ') || (buffer[JONG_SUNG] != ' ') || (tuk != ' ') ||
            (special != " ") || (alphabet != " "))
        {
            ClearBuffer();
        }
        else
        {
            SendText(" ");
        }

        if ( shift == 1 )
        {
            PressShift();
            PressShift();
        }
    }

    public void PressBackSpace()
    {
        SendBackSpace();

        if ( buffer[2] != ' ' )
        {
            char firstJong = new char();
            char secondJong = new char();

            if ( GBCCheck(buffer[2], ref firstJong, ref secondJong) )
            {
                buffer[2] = firstJong;
            }
            else
            {
                buffer[2] = ' ';
            }

            SendText(Hap(buffer[CHO_SUNG], buffer[JUNG_SUNG], buffer[JONG_SUNG]));
        }
        else if ( buffer[1] != ' ' )
        {
            const string jungSung  = "ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣᆢㆍ";
            const string jungSung2 = "ㅣㅏㅏㅑㆍㅓᆢㅕㆍㅚㅘㅗᆢㅡㅠㅝㅜㅜ ㅡ ㆍ ";

            if ( jungSung.Contains(buffer[1]) )
            {
                buffer[1] = jungSung2.Substring(jungSung.IndexOf(buffer[1]), 1)[0];
            }

            var temp = buffer[1];

            if ( "ㆍᆢ".Contains(buffer[1]) && (buffer[0] != ' ') )
            {
                temp = ' ';
            }

            SendText(Hap(buffer[CHO_SUNG], temp, buffer[JONG_SUNG]));
        }
        else
        {
            buffer[CHO_SUNG] = ' ';
            buffer[JUNG_SUNG] = ' ';
            buffer[JONG_SUNG] = ' ';
            tuk = ' ';

            special = " ";
            alphabet = " ";
        }

        if ( shift == 1 )
        {
            PressShift();
            PressShift();
        }
    }

    private void SendBackSpace()
    {
        if ( dummyText.Length > 0 )
        {
            dummyText = dummyText.Substring(0, dummyText.Length - 1);
        }
    }

    public void PressSpecial( int index )
    {
        const string specials = "!?.,()@:;/-♡*_%~^#+×÷=\"'&♤☆♧\\￦<>{}[]`|$€￡￥º○●□■◇※≪≫¤¡¿";

        string s = specials.Substring(index * 2, 2);

        if ( s.Contains(special) )
        {
            s += s.Substring(0, 1);
            special = s.Substring(s.IndexOf(special) + 1, 1);

            SendBackSpace();
        }
        else
        {
            special = s.Substring(0, 1);
        }

        SendText(special);
    }

    public void PressEnglish( int index )
    {
        string alphabets = "abcdefghijklmnopqrstuvwxyz";

        int offset = 0;
        int plus = 0;

        if ( index > 5 )
        {
            offset += 1;
        }

        if ( (index == 5) || (index == 7) )
        {
            plus = 1;
        }

        string a = alphabets.Substring(index * 3 + offset, 3 + plus);

        if ( a.Contains(alphabet.ToLower()) )
        {
            bool upper = false;

            if ( alphabet == alphabet.ToUpper() )
            {
                upper = true;
            }

            a += a.Substring(0, 1);
            alphabet = a.Substring(a.IndexOf(alphabet.ToLower()) + 1, 1);

            if ( upper )
            {
                alphabet = alphabet.ToUpper();
            }

            SendBackSpace();
        }
        else
        {
            alphabet = a.Substring(0, 1);

            if ( shift == 1 )
            {
                var alphabet_ = alphabet;

                PressShift();
                PressShift();

                alphabet = alphabet_.ToUpper();
            }
        }

        if ( shift > 0 )
        {
            alphabet = alphabet.ToUpper();
        }

        SendText(alphabet);
    }

    public void PressShift()
    {
        ClearBuffer();

        if ( shift == 0 )
        {
            shiftText.SetText("△");
            shiftText.fontStyle = FontStyles.Bold;

            english_0.fontStyle = FontStyles.UpperCase;
            english_1.fontStyle = FontStyles.UpperCase;
            english_2.fontStyle = FontStyles.UpperCase;
            english_3.fontStyle = FontStyles.UpperCase;
            english_4.fontStyle = FontStyles.UpperCase;
            english_5.fontStyle = FontStyles.UpperCase;
            english_6.fontStyle = FontStyles.UpperCase;
            english_7.fontStyle = FontStyles.UpperCase;
        }
        else if ( shift == 1 )
        {
            shiftText.SetText("▲");
            shiftText.fontStyle = FontStyles.Normal;
        }
        else if ( shift == 2 )
        {
            shiftText.SetText("△");

            english_0.fontStyle = FontStyles.Normal;
            english_1.fontStyle = FontStyles.Normal;
            english_2.fontStyle = FontStyles.Normal;
            english_3.fontStyle = FontStyles.Normal;
            english_4.fontStyle = FontStyles.Normal;
            english_5.fontStyle = FontStyles.Normal;
            english_6.fontStyle = FontStyles.Normal;
            english_7.fontStyle = FontStyles.Normal;
        }

        shift += 1;
        shift %= 3;
    }
}
