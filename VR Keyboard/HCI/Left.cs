using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Left : MonoBehaviour
{
    public GameObject parent;

    public GameObject keypad_0;
    public GameObject keypad_1;
    public GameObject keypad_2;
    public GameObject keypad_3;

    public GameObject tempSpecial_0;
    public GameObject tempSpecial_1;

    public GameObject shift;

    private int current_index = 3;

    public void Change( int index )
    {
        if ( current_index == index )
        {
            return;
        }

        if ( current_index == 0 )
        {

        }
        else if ( current_index == 1 )
        {
            tempSpecial_0.SetActive(true);
            tempSpecial_1.SetActive(false);
        }
        else if ( current_index == 2 )
        {
            shift.SetActive(false);
        }
        else if ( current_index == 3 )
        {

        }

        current_index = index;

        parent.GetComponent<Cheonjiin>().ClearBuffer();
        
        if ( index == 0 )
        {
            // 특수문자
            keypad_0.SetActive(true);
            keypad_1.SetActive(false);
            keypad_2.SetActive(false);
            keypad_3.SetActive(false);

            keypad_0.GetComponent<Special>().ChangePage(0);
        }
        else if ( index == 1 )
        {
            // 숫자
            keypad_0.SetActive(false);
            keypad_1.SetActive(true);
            keypad_2.SetActive(false);
            keypad_3.SetActive(false);

            tempSpecial_0.SetActive(false);
            tempSpecial_1.SetActive(true);
        }
        else if ( index == 2 )
        {
            // 영어
            keypad_0.SetActive(false);
            keypad_1.SetActive(false);
            keypad_2.SetActive(true);
            keypad_3.SetActive(false);

            shift.SetActive(true);
            parent.GetComponent<Cheonjiin>().InitializeShift();
        }
        else if ( index == 3 )
        {
            // 한글
            keypad_0.SetActive(false);
            keypad_1.SetActive(false);
            keypad_2.SetActive(false);
            keypad_3.SetActive(true);
        }
    }
}
