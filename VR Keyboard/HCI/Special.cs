using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Special : MonoBehaviour
{
    public GameObject page_1;
    public GameObject page_2;
    public GameObject page_3;

    public void ChangePage( int index )
    {
        if ( index == 0 )
        {
            page_1.SetActive(true);
            page_2.SetActive(false);
            page_3.SetActive(false);
        }
        else if ( index == 1 )
        {
            page_1.SetActive(false);
            page_2.SetActive(true);
            page_3.SetActive(false);
        }
        else if ( index == 2 )
        {
            page_1.SetActive(false);
            page_2.SetActive(false);
            page_3.SetActive(true);
        }
    }
}
