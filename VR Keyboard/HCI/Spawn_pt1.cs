using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_pt1 : MonoBehaviour
{
    public GameObject pt1;
    public Cheonjiin cheonjiin;
    public Prototype_1 prototype_1;

    public void Spawn()
    {
        if ( pt1.activeSelf )
        {
            Dispawn();
            return;
        }

        pt1.SetActive(true);
        //pt1.transform.position = transform.position;
        //pt1.transform.rotation = transform.rotation;

        prototype_1.Initialize();
        cheonjiin.Initialize();
    }

    public void Dispawn()
    {
        pt1.SetActive(false);
    }
}
