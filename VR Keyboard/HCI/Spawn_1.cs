using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_1 : MonoBehaviour
{
    public GameObject handle;
    public Cheonjiin cheonjiin;
    public Left left;

    public void Spawn()
    {
        if ( handle.activeSelf )
        {
            Dispawn();
            return;
        }

        handle.SetActive(true);
        handle.transform.position = transform.position;
        handle.transform.rotation = transform.rotation;

        left.Change(2);
        left.Change(3);
        cheonjiin.Initialize();
    }

    public void Dispawn()
    {
        handle.SetActive(false);
    }
}
