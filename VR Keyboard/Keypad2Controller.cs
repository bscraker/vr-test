using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keypad2Controller : MonoBehaviour
{
    public Transform centerEyeTransform;
    public Transform leftControllerTransform;
    public Transform rightControllerTransform;

    public Transform leftTemporalKeypadTransform;
    public Transform rightTemporalKeypadTransform;
    public Transform leftTemporalKeypadPointTransform;
    public Transform rightTemporalKeypadPointTransform;

    public LineRenderer lineRenderer;

    public RectTransform leftPointRectTransform;
    public RectTransform rightPointRectTransform;

    public RectTransform leftSelectedAreaRectTransform;
    public RectTransform rightSelectedAreaRectTransform;

    private bool leftTriggered = false;
    private bool rightTriggered = false;

    private const float RATE = 50f;

    void Update()
    {
        OVRInput.Update();

        var leftRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch).eulerAngles;
        var rightRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTouch).eulerAngles;

        var leftHandTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.Touch);
        var rightHandTrigger = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger, OVRInput.Controller.Touch);
        
        if ( leftHandTrigger > 0.75f )
        {
            if ( !leftTriggered )
            {
                var position = leftControllerTransform.position;
                var direction_ = leftControllerTransform.right.normalized;
                position += (direction_ * 0.5f);

                leftTemporalKeypadTransform.position = position;
                leftTemporalKeypadTransform.LookAt(leftControllerTransform);
            }

            {
                var origin = leftControllerTransform.position;
                var direction = leftControllerTransform.right.normalized;

                origin += (direction * 0.1f);

                var pointer = new Ray(origin, direction);

                lineRenderer.SetPosition(0, origin);

                RaycastHit hit;

                var layer = 6;
                var layerMask = 1 << layer;

                if ( Physics.Raycast(pointer, out hit, 10f, layerMask) )
                {
                    lineRenderer.SetPosition(1, hit.point);

                    leftTemporalKeypadPointTransform.position = hit.point;

                    leftPointRectTransform.anchoredPosition = new Vector2(
                        -leftTemporalKeypadPointTransform.localPosition.x * RATE,
                        -leftTemporalKeypadPointTransform.localPosition.z * RATE
                    );
                }
                else
                {
                    lineRenderer.SetPosition(1, lineRenderer.GetPosition(0));
                }
            }

            var pointPosition = leftPointRectTransform.anchoredPosition;
            pointPosition += new Vector2(301f / 2f, 275f / 2f);

            var x_index = (int)(pointPosition.x / (301f / 3f));
            var y_index = (int)(pointPosition.y / (275f / 4f));
            
            if ( x_index < 0 ) x_index = 0;
            if ( x_index > 2 ) x_index = 2;
            if ( y_index < 0 ) y_index = 0;
            if ( y_index > 3 ) y_index = 3;

            leftSelectedAreaRectTransform.anchoredPosition = new Vector2(
                x_index * (301f / 3f),
                y_index * (275f / 4f) - (275f / 4f * 3f)
            );

            var image = leftSelectedAreaRectTransform.gameObject.GetComponent<Image>();

            if ( OVRInput.Get(OVRInput.Touch.One, OVRInput.Controller.LTouch) )
            {
                image.color = new Color(1f, 1f, 1f, 0.25f);
            }
            else
            {
                image.color = new Color(0f, 0.5f, 1f, 0.25f);
            }

            leftTriggered = true;
        }
        else
        {
            lineRenderer.SetPosition(1, lineRenderer.GetPosition(0));

            leftPointRectTransform.anchoredPosition = new Vector2(
                -50,
                0
            );

            leftTriggered = false;
        }

        if ( rightHandTrigger > 0.75f )
        {
            if ( !rightTriggered )
            {
                var position = rightControllerTransform.position;
                var direction_ = (-rightControllerTransform.right).normalized;
                position += (direction_ * 0.5f);

                rightTemporalKeypadTransform.position = position;
                rightTemporalKeypadTransform.rotation = rightControllerTransform.rotation;
                
                rightTemporalKeypadTransform.LookAt(rightControllerTransform.position);
            }

            {
                var origin = rightControllerTransform.position;
                var direction = (-rightControllerTransform.right).normalized;

                origin += (direction * 0.1f);

                var pointer = new Ray(origin, direction);

                lineRenderer.SetPosition(0, origin);

                RaycastHit hit;

                var layer = 7;
                var layerMask = 1 << layer;

                if ( Physics.Raycast(pointer, out hit, 10f, layerMask) )
                {
                    lineRenderer.SetPosition(1, hit.point);

                    rightTemporalKeypadPointTransform.position = hit.point;

                    rightPointRectTransform.anchoredPosition = new Vector2(
                        -rightTemporalKeypadPointTransform.localPosition.x * RATE,
                        -rightTemporalKeypadPointTransform.localPosition.z * RATE
                    );
                }
                else
                {
                    lineRenderer.SetPosition(1, lineRenderer.GetPosition(0));
                }
            }

            var pointPosition = rightPointRectTransform.anchoredPosition;
            pointPosition += new Vector2(301f / 2f, 275f / 2f);

            var x_index = (int)(pointPosition.x / (301f / 3f));
            var y_index = (int)(pointPosition.y / (275f / 4f));
            
            if ( x_index < 0 ) x_index = 0;
            if ( x_index > 2 ) x_index = 2;
            if ( y_index < 0 ) y_index = 0;
            if ( y_index > 3 ) y_index = 3;

            rightSelectedAreaRectTransform.anchoredPosition = new Vector2(
                x_index * (301f / 3f),
                y_index * (275f / 4f) - (275f / 4f * 3f)
            );

            var image = rightSelectedAreaRectTransform.gameObject.GetComponent<Image>();

            if ( OVRInput.Get(OVRInput.Touch.One, OVRInput.Controller.RTouch) )
            {
                image.color = new Color(1f, 1f, 1f, 0.25f);
            }
            else
            {
                image.color = new Color(0f, 0.5f, 1f, 0.25f);
            }

            rightTriggered = true;
        }
        else
        {
            lineRenderer.SetPosition(1, lineRenderer.GetPosition(0));

            rightPointRectTransform.anchoredPosition = new Vector2(
                50,
                0
            );

            rightTriggered = false;
        }
    }
}
