using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyLocalTransform : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target;

    private void Update()
    {
        var scale = m_Target.lossyScale.x / transform.lossyScale.x;

        transform.localPosition = m_Target.localPosition * scale;
        transform.localRotation = m_Target.localRotation;
    }
}
