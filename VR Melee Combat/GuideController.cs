using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Instance;

    private Action m_Callback = null;

    private TimeoutDestroy m_GuideTimeoutDestroy;
    private AlphaFade m_GuideAlphaFade;
    private GuideTrigger[] m_GuideTriggers = new GuideTrigger[5];

    private void Update()
    {
        if ( m_Callback == null )
        {
            return;
        }

        var success = true;
        var lastTriggeredTime = 0f;

        for ( var i = 0; i < 5; ++i )
        {
            var triggeredTime = m_GuideTriggers[i].GetTriggeredTime();

            if ( (triggeredTime < 0.001f) || (triggeredTime < lastTriggeredTime) )
            {
                success = false;
                break;
            }
        }

        if ( success )
        {
            var callback = m_Callback;

            Disappear();

            callback();
        }
    }

    public void ShowUp( float angle, Action callback )
    {
        Disappear();

        m_Callback = callback;

        var instance = Instantiate(m_Instance, m_Instance.transform.parent);
        m_GuideTimeoutDestroy = instance.GetComponent<TimeoutDestroy>();
        m_GuideAlphaFade = instance.GetComponent<AlphaFade>();

        var child = instance.transform;

        child.localRotation = Quaternion.Euler(0f, 0f, angle);

        for ( var i = 0; i < 5; ++i )
        {
            m_GuideTriggers[i] = child.Find((i + 1).ToString()).GetComponent<GuideTrigger>();
        }

        instance.SetActive(true);
    }

    public void Disappear()
    {
        if ( m_Callback == null )
        {
            return;
        }

        m_Callback = null;

        m_GuideAlphaFade.Initialize();
        m_GuideTimeoutDestroy.StartTimeout();
    }
}
