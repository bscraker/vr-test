using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTriggerVR : MonoBehaviour
{
    [SerializeField]
    private Transform[] m_Points;

    private float m_Radius;

    private Vector3 m_LastPosition;
    private Vector3[] m_LastPointPosition = new Vector3[2];

    private void Awake()
    {
        m_Radius = (m_Points[1].position - m_Points[2].position).magnitude;

        SaveLast();
    }

    private void Update()
    {
        var direction = transform.position - m_LastPosition;
        var v = m_Points[3].position - m_Points[1].position;

        var speed = direction.magnitude / Time.deltaTime;

        if ( (Vector3.Angle(direction, v) > 50f) || (speed < 3f) )
        {
            SaveLast();

            return;
        }
        
        var raycastHits = Physics.CapsuleCastAll(
            m_LastPointPosition[0],
            m_LastPointPosition[1],
            m_Radius,
            direction,
            direction.magnitude,
            (1 << LayerMask.NameToLayer("VROnly"))
        );

        foreach ( var i in raycastHits )
        {
            if ( !i.transform.CompareTag("Guide") )
            {
                continue;
            }

            var parent = i.transform.parent.parent;

            var v1 = m_Points[1].position - m_Points[3].position;
            var v2 = parent.Find("point2").position - parent.Find("point1").position;

            var angle = Vector3.Angle(v1, v2);

            if ( angle > 50f )
            {
                break;
            }

            var guideTrigger = i.transform.parent.GetComponent<GuideTrigger>();

            guideTrigger.Trigger();
        }

        SaveLast();
    }

    private void SaveLast()
    {
        m_LastPosition = transform.position;

        m_LastPointPosition[0] = m_Points[0].position;
        m_LastPointPosition[1] = m_Points[1].position;
    }
}
