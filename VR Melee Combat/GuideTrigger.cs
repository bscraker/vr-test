using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideTrigger : MonoBehaviour
{
    private float m_TriggeredTime = 0f;

    public void Trigger()
    {
        if ( m_TriggeredTime > 0f )
        {
            return;
        }

        var index = int.Parse(name);

        if ( (index - 1) > 0 )
        {
            var prev = transform.parent.Find((index - 1).ToString()).GetComponent<GuideTrigger>();
            
            if ( prev.GetTriggeredTime() < 0.001f )
            {
                return;
            }
        }

        foreach ( Transform child in transform )
        {
            var color = child.GetComponent<MeshRenderer>().material.color;
            color.r = 0f;
            color.g = 0.5f;

            child.GetComponent<MeshRenderer>().material.color = color;
        }

        m_TriggeredTime = Time.time;
    }

    public float GetTriggeredTime()
    {
        return m_TriggeredTime;
    }
}
