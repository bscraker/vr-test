using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTriggerVR_A : MonoBehaviour
{
    [SerializeField]
    private Transform[] m_Points;

    [SerializeField]
    private GameObject m_EffectInstance;

    [SerializeField]
    private GameObject m_Miss;

    private float m_Radius;

    private Vector3 m_LastPosition;
    private Vector3[] m_LastPointPosition = new Vector3[2];

    private float m_LastAttackTime = 0f;

    private void Awake()
    {
        m_Radius = (m_Points[1].position - m_Points[2].position).magnitude;

        SaveLast();
    }

    private void Update()
    {
        var direction = transform.position - m_LastPosition;
        var v = m_Points[3].position - m_Points[1].position;

        var speed = direction.magnitude / Time.deltaTime;

        if ( (Vector3.Angle(direction, v) > 50f) || (speed < 10f) )
        {
            SaveLast();

            return;
        }

        var raycastHits = Physics.CapsuleCastAll(
            m_LastPointPosition[0],
            m_LastPointPosition[1],
            m_Radius,
            direction,
            direction.magnitude,
            (1 << LayerMask.NameToLayer("Default"))
        );

        foreach ( var i in raycastHits )
        {
            if ( !i.transform.CompareTag("NPC") )
            {
                continue;
            }

            var time = Time.time - m_LastAttackTime;

            if ( time < 0.8f )
            {
                if ( time > 0.2f )
                {
                    var miss = Instantiate(m_Miss);
                    miss.transform.position = m_Miss.transform.position;
                    miss.SetActive(true);
                }

                m_LastAttackTime = Time.time;

                break;
            }

            var npcController = i.transform.GetComponent<NPCController>();
            npcController.TakeDamage();
            
            var instance = Instantiate(m_EffectInstance);

            if ( (i.distance == 0f) && (i.point.magnitude == 0f) )
            {
                instance.transform.position = (m_LastPointPosition[0] + m_LastPointPosition[1]) / 2f;
            }
            else
            {
                instance.transform.position = i.point;
            }

            instance.SetActive(true);

            m_LastAttackTime = Time.time;

            break;
        }

        SaveLast();
    }

    private void SaveLast()
    {
        m_LastPosition = transform.position;

        m_LastPointPosition[0] = m_Points[0].position;
        m_LastPointPosition[1] = m_Points[1].position;
    }
}
