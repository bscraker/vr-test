using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class SettingUpdater : MonoBehaviour
{
    [SerializeField]
    private Slider[] m_Sliders;

    [SerializeField]
    private GameObject[] m_VignettingObjects;

    private TextMeshProUGUI m_TextMeshPro;
    private CustomLocomotionProvider m_CustomLocomotionProvider;
    private Vignetting m_Vignetting;

    private void Awake()
    {
        m_TextMeshPro = GetComponent<TextMeshProUGUI>();
        m_CustomLocomotionProvider = GameObject.Find("Locomotion System").GetComponent<CustomLocomotionProvider>();
        m_Vignetting = GameObject.Find("Vignetting").GetComponent<Vignetting>();
    }

    private void Update()
    {
        // ***�ϵ��ڵ�***

        var text = "\n";
        
        text += m_Sliders[0].value.ToString() + "\n";
        m_CustomLocomotionProvider.m_MoveSpeed = m_Sliders[0].value;

        if ( m_Sliders[1].value == 0f )
        {
            text += "Continuous\n";
            m_CustomLocomotionProvider.m_TurnType = CustomLocomotionProvider.TurnType.Continuous;
        }
        else
        {
            text += "Snap\n";
            m_CustomLocomotionProvider.m_TurnType = CustomLocomotionProvider.TurnType.Snap;
        }

        text += m_Sliders[2].value.ToString() + "\n";
        m_CustomLocomotionProvider.m_TurnSpeed = m_Sliders[2].value;

        text += m_Sliders[3].value.ToString() + "\n";
        m_CustomLocomotionProvider.m_TurnAmount = m_Sliders[3].value;

        text += m_Sliders[4].value.ToString() + "\n";
        m_CustomLocomotionProvider.m_JumpSpeed = m_Sliders[4].value;

        text += "\n";

        switch ( m_Sliders[5].value )
        {
            case 0f:
                text += "\nOff";
                break;

            case 1f:
                text += "\nWhite";
                break;

            case 2f:
                text += "\nBlack";
                break;

            case 3f:
                text += "\nRoom (Bright)";
                break;

            case 4f:
                text += "\nRoom (Dark)";
                break;

            default:
                break;
        }

        for ( int i = 0; i < 4; ++i )
        {
            m_VignettingObjects[i].SetActive(i == (m_Sliders[5].value - 1));
        }

        text += "\n" + m_Sliders[6].value.ToString();
        m_Vignetting.m_CutoutSize = m_Sliders[6].value;

        m_TextMeshPro.text = text;
    }
}
