using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class NPCController : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private AudioSource m_Impact;

    [SerializeField]
    private XRBaseController[] m_XRControllers;

    [SerializeField]
    private GameObject m_Hit;

    private int m_DamageCount = 0;

    private int m_LastDamageIndex = 0;
    private int m_LastDeadIndex = 0;

    private void Awake()
    {
        m_LastDamageIndex = Random.Range(0, 3);
        m_LastDeadIndex = Random.Range(0, 3);
    }

    public void TakeDamage()
    {
        if ( (m_DamageCount == 0) && !m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") )
        {
            return;
        }

        ++m_DamageCount;

        if ( m_DamageCount < 5 )
        {
            var index = Random.Range(0, 2);
            index += m_LastDamageIndex + 1;
            index %= 3;

            m_Animator.SetTrigger("Damage" + (index + 1).ToString());
        }
        else
        {
            var index = Random.Range(0, 2);
            index += m_LastDamageIndex + 1;
            index %= 3;

            m_Animator.SetTrigger("Dead" + (index + 1).ToString());

            m_DamageCount = 0;
        }

        var hit = Instantiate(m_Hit);
        hit.transform.position = m_Hit.transform.position;
        hit.SetActive(true);

        m_Impact.Stop();
        m_Impact.Play();

        SendHapticImpulse(1f, 0.1f);
    }

    private void SendHapticImpulse( float amplitude, float duration )
    {
        foreach ( var i in m_XRControllers )
        {
            i.SendHapticImpulse(amplitude, duration);
        }
    }
}
