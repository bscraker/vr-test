using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedRotation : MonoBehaviour
{
    private Quaternion m_Rotation;

    private void Awake()
    {
        m_Rotation = transform.rotation;
    }

    private void Update()
    {
        transform.rotation = m_Rotation;
    }
}
