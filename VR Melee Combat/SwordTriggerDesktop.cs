using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SwordTriggerDesktop : MonoBehaviour
{
    [SerializeField]
    private Transform[] m_Points;

    [SerializeField]
    private GameObject m_EffectInstance;

    private float m_Radius;

    private Vector3 m_LastPosition;
    private Vector3[] m_LastPointPosition = new Vector3[2];

    private float m_CurrentAttackTime;
    private float m_LastAttackTime;

    private float m_Timeout;

    private void Awake()
    {
        m_Radius = (m_Points[1].position - m_Points[2].position).magnitude;

        SaveLast();

        m_LastAttackTime = m_CurrentAttackTime;
    }

    private void Update()
    {
        if ( (m_LastAttackTime == m_CurrentAttackTime) || (m_CurrentAttackTime > Time.time) || (m_CurrentAttackTime + m_Timeout < Time.time) )
        {
            SaveLast();

            return;
        }

        var direction = transform.position - m_LastPosition;

        var raycastHits = Physics.CapsuleCastAll(
            m_LastPointPosition[0],
            m_LastPointPosition[1],
            m_Radius,
            direction,
            direction.magnitude,
            (1 << LayerMask.NameToLayer("Default"))
        );

        foreach ( var i in raycastHits )
        {
            if ( !i.transform.CompareTag("NPC") )
            {
                continue;
            }
            
            var npcController = i.transform.GetComponent<NPCController>();
            npcController.TakeDamage();
            
            var instance = Instantiate(m_EffectInstance);

            if ( (i.distance == 0f) && (i.point.magnitude == 0f) )
            {
                instance.transform.position = (m_LastPointPosition[0] + m_LastPointPosition[1]) / 2f;
            }
            else
            {
                instance.transform.position = i.point;
            }

            instance.SetActive(true);

            m_LastAttackTime = m_CurrentAttackTime;

            break;
        }

        SaveLast();
    }

    private void SaveLast()
    {
        m_LastPosition = transform.position;

        m_LastPointPosition[0] = m_Points[0].position;
        m_LastPointPosition[1] = m_Points[1].position;
    }

    public void SetAttackTime( float offset, float timeout )
    {
        m_CurrentAttackTime = Time.time + offset;
        m_Timeout = timeout;
    }

    public void ResetAttackTime()
    {
        m_LastAttackTime = m_CurrentAttackTime;
    }
}
