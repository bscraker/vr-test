using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuideOffset : MonoBehaviour
{
    [SerializeField]
    private Transform m_MainCamera;

    [SerializeField]
    private Transform m_CameraOffset;

    private float m_YOffset = 0f;

    private void Awake()
    {
        m_YOffset = transform.localPosition.y;
    }

    private void Update()
    {
        var localPosition = transform.localPosition;
        localPosition.y = m_YOffset;
        localPosition.y += (m_MainCamera.position.y - m_CameraOffset.position.y);

        transform.localPosition = localPosition;

        transform.LookAt(m_MainCamera);

        var localRotation = transform.localRotation.eulerAngles;
        localRotation.x = 0f;

        transform.localRotation = Quaternion.Euler(localRotation);
    }
}
