using System.Collections;
using Unity.XR.CoreUtils;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Windows;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using UnityEngine.XR.Interaction.Toolkit.Inputs.Simulation;

namespace UnityEngine.XR.Interaction.Toolkit
{
    public class CustomLocomotionProvider : LocomotionProvider
    {
        public enum TurnType
        {
            Continuous,
            Snap
        }

        [SerializeField]
        private Transform m_CameraOffset;

        [SerializeField]
        private Transform m_AttackCameraOffset;

        [SerializeField]
        private Transform m_ForwardSource;

        public bool m_EnableMove = true;
        public float m_MoveSpeed = 5f;

        public bool m_EnableTurn = true;
        public TurnType m_TurnType = TurnType.Snap;
        public float m_TurnSpeed = 60f;
        public float m_TurnAmount = 45f;
        public bool m_EnableTurnAround = true;

        public bool m_EnableJump = true;
        public float m_JumpSpeed = 4f;

        [SerializeField]
        private InputActionProperty m_LeftHandAction;

        [SerializeField]
        private InputActionProperty m_RightHandAction;

        [SerializeField]
        private InputActionProperty m_LeftHandSelectAction;

        [SerializeField]
        private InputActionProperty m_RightHandSelectAction;

        [SerializeField]
        private InputActionProperty m_RightHandPrimaryButtonAction;

        [SerializeField]
        private InputActionProperty m_RightHandSecondaryButtonAction;

        [SerializeField]
        private Animator m_Animator;

        [SerializeField]
        private GameObject m_Sword_VR;

        [SerializeField]
        private GameObject m_Sword_Desktop;

        [SerializeField]
        private MeleeWeaponTrail m_Trail_VR;

        [SerializeField]
        private MeleeWeaponTrail m_Trail_VR_A;

        [SerializeField]
        private MeleeWeaponTrail m_Trail_Desktop;

        [SerializeField]
        private AudioSource m_SlashAudio;

        [SerializeField]
        private Transform m_CharacterCenterEye;

        [SerializeField]
        private GuideController m_GuideController;

        [SerializeField]
        private GameObject[] m_EnableObjectsOnAttack;

        [SerializeField]
        private GameObject[] m_DisableObjectsOnAttack;

        [SerializeField]
        private XRRayInteractor[] m_DisableXRRayInteratorOnAttack;

        [SerializeField]
        private GameObject[] m_EnableObjectsOnOffsetSetup;

        [SerializeField]
        private GameObject[] m_DisableObjectsOnOffsetSetup;

        [SerializeField]
        private SwordTriggerDesktop m_SwordTriggerDesktop;

        [SerializeField]
        private Transform m_LeftHandTransform;

        [SerializeField]
        private Transform m_RightHandTransform;

        [SerializeField]
        private Transform m_LeftHandIKTransform;

        [SerializeField]
        private Transform m_RightHandIKTransform;

        [SerializeField]
        private AudioSource m_RunAudio;

        [SerializeField]
        private AudioSource m_JumpAudio;

        [SerializeField]
        private AudioSource m_LandAudio;

        [SerializeField]
        private GameObject m_GuideA;

        [SerializeField]
        private GameObject m_GuideB;

        [SerializeField]
        private AudioSource m_SelectAudio;

        [SerializeField]
        private GameObject m_AttackASword;

        private enum AttackState
        {
            Idle,
            AttackReady,
            Unknown
        }

        private CharacterController m_CharacterController;

        private bool m_IsTurned = false;

        private Vector3 m_MotionMove = Vector3.zero;
        private Vector3 m_MotionJump = Vector3.zero;
        private Vector3 m_MotionSlip = Vector3.zero;

        private bool m_IsGrounded = false;
        private bool m_IsSlipped = false;
        private bool m_IsJumped = false;

        private float m_SavedSlopeLimit = 0f;
        private float m_SavedStepOffset = 0f;

        private Coroutine m_UpdateCameraCoroutine = null;
        private Coroutine m_ShakeCameraCoroutine = null;

        private bool m_AnimatorLocomotionFlag = false;
        private float m_AnimatorDestVelocity = 0f;
        private Vector2 m_AnimatorDestXY = Vector2.zero;

        private bool m_IsAttackReady = false;
        private int m_LastAttackIndex = 0;
        private Coroutine m_AttackCoroutin = null;

        private Vector3 m_AttackMove = Vector3.zero;
        private Coroutine m_AttackMoveCoroutin = null;

        private bool m_IsOffsetSetup = false;
        private Transform m_OffsetLeftParent;
        private Transform m_OffsetRightParent;

        private float m_FootstepTime = -1f;

        private bool m_GuideABPressed = false;
        private int m_GuideABIndex = 0;

        private bool m_AttackA = false;

        protected override void Awake()
        {
            base.Awake();

            m_CharacterController = system.xrOrigin.Origin.GetComponent<CharacterController>();
            m_SavedSlopeLimit = m_CharacterController.slopeLimit;
            m_SavedStepOffset = m_CharacterController.stepOffset;

            m_Trail_VR.Use = false;
            m_Trail_VR_A.Use = false;
            m_Trail_Desktop.Use = false;

            m_OffsetLeftParent = m_LeftHandTransform.parent;
            m_OffsetRightParent = m_RightHandTransform.parent;
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        private void Update()
        {
            var check = false;

            check = UpdateOffsetSetup();

            if ( !check )
            {
                UpdateGuideAB();
            }

            var isMoving = false;

            isMoving |= UpdateMove();
            isMoving |= UpdateTurn();

            UpdateLocomotionPhase(isMoving);

            UpdateAnimator(0.2f);
            UpdateAttackA(0.2f);
            UpdateAttackB(0.2f);

#if UNITY_EDITOR
            // Test
            if ( Input.GetKeyDown(KeyCode.L) )
            {
                Attack(0);
            }
            else if ( Input.GetKeyDown(KeyCode.Semicolon) )
            {
                Attack(1);
            }
            else if ( Input.GetKeyDown(KeyCode.Quote) )
            {
                Attack(2);
            }
#endif
        }
        
        private Vector3 ComputeDesiredMove(Vector2 input)
        {
            if (input == Vector2.zero)
                return Vector3.zero;

            var xrOrigin = system.xrOrigin;
            if (xrOrigin == null)
                return Vector3.zero;

            // Assumes that the input axes are in the range [-1, 1].
            // Clamps the magnitude of the input direction to prevent faster speed when moving diagonally,
            // while still allowing for analog input to move slower (which would be lost if simply normalizing).
            var inputMove = Vector3.ClampMagnitude(new Vector3(input.x, 0f, input.y), 1f);

            var originTransform = xrOrigin.Origin.transform;
            var originUp = originTransform.up;

            // Determine frame of reference for what the input direction is relative to
            var forwardSourceTransform = m_ForwardSource == null ? xrOrigin.Camera.transform : m_ForwardSource;
            var inputForwardInWorldSpace = forwardSourceTransform.forward;
            if (Mathf.Approximately(Mathf.Abs(Vector3.Dot(inputForwardInWorldSpace, originUp)), 1f))
            {
                // When the input forward direction is parallel with the rig normal,
                // it will probably feel better for the player to move along the same direction
                // as if they tilted forward or up some rather than moving in the rig forward direction.
                // It also will probably be a better experience to at least move in a direction
                // rather than stopping if the head/controller is oriented such that it is perpendicular with the rig.
                inputForwardInWorldSpace = -forwardSourceTransform.up;
            }

            var inputForwardProjectedInWorldSpace = Vector3.ProjectOnPlane(inputForwardInWorldSpace, originUp);
            var forwardRotation = Quaternion.FromToRotation(originTransform.forward, inputForwardProjectedInWorldSpace);

            var translationInRigSpace = forwardRotation * inputMove * m_MoveSpeed;
            var translationInWorldSpace = originTransform.TransformDirection(translationInRigSpace);

            return translationInWorldSpace;
        }

        private float GetTurnAmount( Vector2 input )
        {
            if ( input == Vector2.zero )
            {
                m_IsTurned = false;
                return 0f;
            }

            var cardinal = CardinalUtility.GetNearestCardinal(input);

            switch ( cardinal )
            {
                case Cardinal.North:
                    break;

                case Cardinal.South:
                    if ( m_EnableTurnAround && !m_IsTurned )
                    {
                        m_IsTurned = true;
                        return 180f;
                    }

                    break;

                case Cardinal.East:
                case Cardinal.West:
                    if ( m_TurnType == TurnType.Continuous )
                    {
                        return input.magnitude * (Mathf.Sign(input.x) * m_TurnSpeed * Time.deltaTime);
                    }
                    else if ( m_TurnType == TurnType.Snap && !m_IsTurned  )
                    {
                        m_IsTurned = true;
                        return Mathf.Sign(input.x) * m_TurnAmount;
                    }

                    break;

                default:
                    Assert.IsTrue(false, $"Unhandled {nameof(Cardinal)}={cardinal}");
                    break;
            }

            return 0f;
        }

        private bool UpdateMove()
        {
            var isGround = false;

            if ( m_IsJumped && (m_MotionJump.y > 0f) )
            {
                if ( CheckCeiling() )
                {
                    m_MotionJump = Vector3.zero;

                    isGround = IsGround();
                }
            }
            else
            {
                isGround = IsGround();
            }

            if ( isGround )
            {
                if ( !m_IsGrounded && !m_IsSlipped )
                {
                    if ( m_UpdateCameraCoroutine != null )
                    {
                        StopCoroutine(m_UpdateCameraCoroutine);
                    }

                    if ( m_ShakeCameraCoroutine != null )
                    {
                        StopCoroutine(m_ShakeCameraCoroutine);
                    }

                    var offset = new Vector3();
                    offset.y = m_MotionJump.y * Time.deltaTime;

                    m_UpdateCameraCoroutine = StartCoroutine(UpdateCameraOffset(offset, 0.2f));
                    m_ShakeCameraCoroutine = StartCoroutine(ShakeCamera(offset.magnitude * 5f, 0.2f));

                    var rate = offset.magnitude / ((Physics.gravity * Time.deltaTime).magnitude);
                    rate = Mathf.Clamp01(rate / 0.35f);

                    m_LandAudio.volume = rate;
                    m_LandAudio.Stop();
                    m_LandAudio.Play();
                }

                m_MotionJump = 100f * Vector3.down;

                m_IsSlipped = false;

                m_CharacterController.slopeLimit = m_SavedSlopeLimit;
                m_CharacterController.stepOffset = m_SavedStepOffset;

            }
            else
            {
                if ( m_IsGrounded )
                {
                    m_MotionJump = Vector3.zero;

                    m_Animator.SetTrigger("MidAir");
                }

                var isSlipped = (m_MotionSlip != Vector3.zero);

                if ( isSlipped )
                {
                    m_MotionJump.y = -100f;
                    m_MotionJump += m_MotionSlip;
                }
                else if ( !isSlipped )
                {
                    if ( m_IsSlipped )
                    {
                        m_MotionJump.y = 0f;
                    }

                    m_MotionJump += (Physics.gravity * Time.deltaTime);
                }

                m_IsSlipped = isSlipped;

                m_CharacterController.slopeLimit = 90f;
                m_CharacterController.stepOffset = 0f;
            }

            m_IsGrounded = isGround;

            UpdateJump();

            m_Animator.SetBool("Land", m_IsGrounded);

            var input = Vector2.zero;

            if ( m_EnableMove )
            {
                input = m_LeftHandAction.action?.ReadValue<Vector2>() ?? Vector2.zero;

                m_AnimatorDestXY = CircleToSquare(input);
            }

            var value = ComputeDesiredMove(input);

            if ( isGround )
            {
                m_MotionMove = value;
            }
            else
            {
                m_MotionMove += 2.5f * Time.deltaTime * value;

                if ( m_MotionMove.magnitude > m_MoveSpeed )
                {
                    m_MotionMove = m_MotionMove.normalized * m_MoveSpeed;
                }
            }

            var motion = m_MotionMove;

            if ( motion.magnitude > m_MoveSpeed )
            {
                motion = motion.normalized * m_MoveSpeed;
            }
            else
            {
                motion += m_MotionJump;
                motion.y = 0;

                if ( motion.magnitude > m_MoveSpeed )
                {
                    motion = motion.normalized * m_MoveSpeed;
                }
            }

            motion += m_AttackMove;

            if ( CanBeginLocomotion() && BeginLocomotion() )
            {
                if ( m_IsSlipped )
                {
                    m_CharacterController.Move((Vector3.up * m_MotionJump.y) * Time.deltaTime);
                    m_CharacterController.Move(motion * Time.deltaTime);
                }
                else
                {
                    motion.y = m_MotionJump.y;

                    m_CharacterController.Move(motion * Time.deltaTime);
                }
                
                if ( m_IsGrounded )
                {
                    motion.y = 0f;

                    m_AnimatorDestVelocity = motion.magnitude / m_MoveSpeed;

                    if ( !m_AnimatorLocomotionFlag )
                    {
                        var locomotion = false;

                        if ( !m_Animator.GetBool("Attack") && !m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Locomotion") )
                        {
                            m_AnimatorLocomotionFlag = true;
                            m_Animator.SetTrigger("Locomotion");

                            StartCoroutine(Callback(
                                () => { m_AnimatorLocomotionFlag = false; },
                                0.25f
                            ));

                            locomotion = true;
                        }
                        else if ( m_Animator.GetBool("Attack") && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Locomotion") )
                        {
                            locomotion = true;
                        }

                        if ( locomotion )
                        {
                            SetSwordVisible(false);

                            m_Trail_Desktop.Use = false;
                        }
                    }

                    if ( (m_AnimatorDestVelocity > 0f) && !m_IsAttackReady )
                    {
                        var time = Time.time;
                        var footstepOffset = 0.4f + (1f - m_AnimatorDestVelocity);
                        var nextFootstepTime = time + footstepOffset;

                        if ( (m_FootstepTime < 0f) || (nextFootstepTime < m_FootstepTime) )
                        {
                            m_FootstepTime = nextFootstepTime;
                        }

                        if ( (m_FootstepTime > 0f) && (time >= m_FootstepTime) )
                        {
                            m_FootstepTime = -1f;

                            m_RunAudio.Stop();
                            m_RunAudio.Play();
                        }
                    }
                }

                EndLocomotion();

                return true;
            }

            return false;
        }
        
        private Vector2 CircleToSquare( Vector2 vec2 )
        {
            var u = vec2.x;
            var v = vec2.y;

            return new Vector2(
                Mathf.Sqrt(2 + (u * u) - (v * v) + (2 * u * Mathf.Sqrt(2)))/2
                - Mathf.Sqrt(2 + (u * u) - (v * v) - (2 * u * Mathf.Sqrt(2))) / 2,
                Mathf.Sqrt(2 - (u * u) + (v * v) + (2 * v * Mathf.Sqrt(2))) / 2
                - Mathf.Sqrt(2 - (u * u) + (v * v) - (2 * v * Mathf.Sqrt(2))) / 2
            );
        }

        private bool IsGround()
        {
            m_MotionSlip = Vector3.zero;

            var radius = m_CharacterController.radius;
            var offset = m_CharacterController.skinWidth;

            var xrOrigin = system.xrOrigin.Origin.transform;

            var origin = xrOrigin.position;
            origin += Vector3.up * (offset + radius);
            origin += xrOrigin.right * m_CharacterController.center.x;
            origin += xrOrigin.forward * m_CharacterController.center.z;

            // TODO: raycast 레이어 설정
            if ( Physics.Raycast(origin, Vector3.down, out RaycastHit hit1) )
            {
                var max = (radius + offset) / Mathf.Cos(m_SavedSlopeLimit * Mathf.Deg2Rad);

                if ( hit1.distance <= max )
                {
                    return true;
                }
                else if ( Physics.SphereCast(origin, radius, Vector3.down, out RaycastHit hit2, offset) )
                {
                    var slip = origin - hit2.point;
                    slip.y = 0f;

                    m_MotionSlip = slip;
                }
            }
            else
            {
                Assert.IsTrue(false, "Unhandled IsGround()");
            }

            return false;
        }

        private bool UpdateTurn()
        {
            if ( !m_EnableTurn )
            {
                return false;
            }

            var input = m_RightHandAction.action?.ReadValue<Vector2>() ?? Vector2.zero;
            var value = GetTurnAmount(input);

            if ( value != 0f )
            {
                if ( CanBeginLocomotion() && BeginLocomotion() )
                {
                    system.xrOrigin.RotateAroundCameraUsingOriginUp(value);
                    EndLocomotion();

                    return true;
                }
            }
            
            return false;
        }

        private bool CheckCeiling()
        {
            var radius = m_CharacterController.radius;
            var offset = m_CharacterController.skinWidth;

            var origin = system.xrOrigin.Origin.transform.position;
            origin += Vector3.up * (offset + m_CharacterController.height - radius);

            return Physics.SphereCast(origin, radius, Vector3.up, out RaycastHit _, offset);
        }

        private bool GetJump( Vector2 input )
        {
            if ( input == Vector2.zero )
            {
                return false;
            }

            var cardinal = CardinalUtility.GetNearestCardinal(input);

            return (cardinal == Cardinal.North);
        }

        private void UpdateJump()
        {
            if ( !m_EnableJump || !m_IsGrounded )
            {
                return;
            }

            var input = m_RightHandAction.action?.ReadValue<Vector2>() ?? Vector2.zero;
            var value = GetJump(input);

            if ( value )
            {
                if ( !m_IsJumped )
                {
                    m_MotionJump = Vector3.up * m_JumpSpeed;

                    m_SavedSlopeLimit = m_CharacterController.slopeLimit;
                    m_SavedStepOffset = m_CharacterController.stepOffset;

                    m_CharacterController.slopeLimit = 90f;
                    m_CharacterController.stepOffset = 0f;

                    m_IsGrounded = false;
                    m_IsJumped = true;

                    m_Animator.SetTrigger("MidAir");

                    m_JumpAudio.Stop();
                    m_JumpAudio.Play();
                }
            }
            else if ( m_IsJumped )
            {
                m_IsJumped = false;
            }
        }

        private void UpdateLocomotionPhase( bool isMoving )
        {
            switch ( locomotionPhase )
            {
                case LocomotionPhase.Idle:
                case LocomotionPhase.Started:
                    if ( isMoving )
                        locomotionPhase = LocomotionPhase.Moving;
                    break;

                case LocomotionPhase.Moving:
                    if ( !isMoving )
                        locomotionPhase = LocomotionPhase.Done;
                    break;

                case LocomotionPhase.Done:
                    locomotionPhase = isMoving ? LocomotionPhase.Moving : LocomotionPhase.Idle;
                    break;

                default:
                    Assert.IsTrue(false, $"Unhandled {nameof(LocomotionPhase)}={locomotionPhase}");
                    break;
            }
        }

        private IEnumerator UpdateCameraOffset( Vector3 offset, float duration )
        {
            var time = Time.time;
            var localPosition = m_CameraOffset.localPosition;

            m_CameraOffset.localPosition = (localPosition + offset);

            float t;

            do
            {
                yield return null;

                t = Time.time - time;
                t = Mathf.Clamp01(t / duration);

                m_CameraOffset.localPosition = (localPosition + Vector3.Lerp(offset, Vector3.zero, t));
            }
            while ( t < 1f );
        }

        private IEnumerator ShakeCamera( float amount, float duration )
        {
            var time = Time.time;
            var localRotation = m_CameraOffset.localRotation.eulerAngles;
            var forward = Camera.main.transform.forward;

            m_CameraOffset.localRotation = Quaternion.Euler(localRotation + (forward * amount));

            float t;

            do
            {
                yield return null;

                t = Time.time - time;
                t = Mathf.Clamp01(t / duration);

                if ( t < 0.5f )
                {
                    m_CameraOffset.localRotation = Quaternion.Euler(localRotation + Vector3.Lerp((forward * amount), (forward * -amount), (t * 2)));
                }
                else
                {
                    m_CameraOffset.localRotation = Quaternion.Euler(localRotation + Vector3.Lerp((forward * -amount), Vector3.zero, ((t - 0.5f) * 2)));
                }
            }
            while ( t < 1f );
        }

        private void UpdateAnimator( float duration )
        {
            var t = Mathf.Clamp01(Time.deltaTime / duration);

            var srcVelocity = m_Animator.GetFloat("Velocity");
            var velocity = Mathf.Lerp(srcVelocity, m_AnimatorDestVelocity, t);

            if ( Mathf.Abs(velocity - m_AnimatorDestVelocity) < 0.001f )
            {
                velocity = m_AnimatorDestVelocity;
            }

            m_Animator.SetFloat("Velocity", velocity);

            var srcXY = new Vector2(m_Animator.GetFloat("Horizontal"), m_Animator.GetFloat("Vertical"));
            var xy = Vector2.Lerp(srcXY, m_AnimatorDestXY, t);
            
            if ( Mathf.Abs(xy.magnitude - m_AnimatorDestXY.magnitude) < 0.001f )
            {
                xy = m_AnimatorDestXY;
            }

            m_Animator.SetFloat("Horizontal", xy.x);
            m_Animator.SetFloat("Vertical", xy.y);
        }

        private IEnumerator Callback( System.Action callback, float time )
        {
            yield return new WaitForSeconds(time);

            callback();
        }

        private void SetEnableLocomotion( bool enable )
        {
            m_EnableMove = enable;
            m_EnableTurn = enable;
            m_EnableJump = enable;
        }

        private void SetSwordVisible( bool isVisible )
        {
            m_Animator.SetBool("Attack", isVisible);

            if ( isVisible )
            {
                m_Sword_Desktop.layer = LayerMask.NameToLayer("Default");
            }
            else
            {
                m_Sword_Desktop.layer = LayerMask.NameToLayer("DesktopOnly");
            }
        }

        private AttackState GetAttackState()
        {
            var currentAnimator = m_Animator.GetCurrentAnimatorStateInfo(0);

            if ( m_IsOffsetSetup )
            {
                return AttackState.Unknown;
            }
            else if ( (currentAnimator.IsName("Locomotion") || currentAnimator.IsName("JumpMidAir") || currentAnimator.IsName("JumpLand")) && m_Animator.GetBool("Land") )
            {
                if ( m_IsAttackReady )
                {
                    return AttackState.AttackReady;
                }
                else
                {
                    return AttackState.Idle;
                }
            }
            else if ( currentAnimator.IsName("Combo1") || currentAnimator.IsName("Combo2") || currentAnimator.IsName("Combo3") )
            {
                return AttackState.AttackReady;
            }

            return AttackState.Unknown;
        }

        private void Attack( int attackIndex )
        {
            if ( GetAttackState() != AttackState.AttackReady )
            {
                return;
            }

            if ( !m_AnimatorLocomotionFlag )
            {
                m_AnimatorLocomotionFlag = true;

                StartCoroutine(Callback(
                    () => { m_AnimatorLocomotionFlag = false; },
                    0.25f
                ));

                m_Trail_Desktop.Use = true;

                SetSwordVisible(true);
            }

            var input = m_LeftHandAction.action?.ReadValue<Vector2>() ?? Vector2.zero;
            var isForward = GetJump(input);

            switch ( attackIndex )
            {
                case 0:
                    m_Animator.SetTrigger("Combo1");
                    m_Animator.ResetTrigger("Combo2");
                    m_Animator.ResetTrigger("Combo3");

                    if ( m_AttackMoveCoroutin != null )
                    {
                        StopCoroutine(m_AttackMoveCoroutin);
                        m_AttackMove = Vector2.zero;
                    }

                    m_SlashAudio.Stop();
                    m_SlashAudio.Play();

                    m_SwordTriggerDesktop.SetAttackTime(0.45f, 0.3f);

                    if ( isForward )
                    {
                        m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                            {
                                m_AttackMove = ComputeDesiredMove(Vector2.up) * 2f;

                                m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                                {
                                    m_AttackMove = Vector2.zero;

                                    m_AttackMoveCoroutin = null;
                                },
                                    0.15f
                                ));
                            },
                            0.3f
                        ));
                    }

                    break;

                case 1:
                    m_Animator.ResetTrigger("Combo1");
                    m_Animator.SetTrigger("Combo2");
                    m_Animator.ResetTrigger("Combo3");

                    if ( m_AttackMoveCoroutin != null )
                    {
                        StopCoroutine(m_AttackMoveCoroutin);
                        m_AttackMove = Vector2.zero;
                    }

                    m_SlashAudio.Stop();
                    m_SlashAudio.Play();

                    m_SwordTriggerDesktop.SetAttackTime(0.6f, 0.13f);
                    
                    if ( isForward )
                    {
                        m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                            {
                                m_AttackMove = ComputeDesiredMove(Vector2.up) * 1.5f;

                                m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                                {
                                    m_AttackMove = Vector2.zero;

                                    m_AttackMoveCoroutin = null;
                                },
                                    0.45f
                                ));
                            },
                            0.15f
                        ));
                    }

                    break;

                case 2:
                    m_Animator.ResetTrigger("Combo1");
                    m_Animator.ResetTrigger("Combo2");
                    m_Animator.SetTrigger("Combo3");

                    if ( m_AttackMoveCoroutin != null )
                    {
                        StopCoroutine(m_AttackMoveCoroutin);
                        m_AttackMove = Vector2.zero;
                    }

                    m_SlashAudio.Stop();
                    m_SlashAudio.Play();

                    m_SwordTriggerDesktop.SetAttackTime(0.585f, 0.13f);
                    
                    if ( isForward )
                    {
                        m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                            {
                                m_AttackMove = ComputeDesiredMove(Vector2.up) * 2f;

                                m_AttackMoveCoroutin = StartCoroutine(Callback(() =>
                                {
                                    m_AttackMove = Vector2.zero;

                                    m_AttackMoveCoroutin = null;
                                },
                                    0.35f
                                ));
                            },
                            0.2f
                        ));
                    }

                    break;

                default:
                    break;
            }
        }

        private void UpdateAttackB( float duration )
        {
            if ( m_GuideABIndex != 1 )
            {
                return;
            }

            var t = Mathf.Clamp01(Time.deltaTime / duration);

            var attackState = GetAttackState();

            var input = m_RightHandSelectAction.action?.ReadValue<float>() ?? 0f;
            var pressed = (input > 0.75f);

            if ( pressed && (attackState == AttackState.Idle) )
            {
                m_IsAttackReady = true;

                m_Sword_VR.SetActive(true);
                m_Trail_VR.Use = true;

                m_Sword_Desktop.SetActive(true);

                AttackGuide(m_LastAttackIndex);

                SetEnableLocomotion(false);
                
                foreach ( var i in m_EnableObjectsOnAttack )
                {
                    i.SetActive(true);
                }
                
                foreach ( var i in m_DisableObjectsOnAttack )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = false;
                }
            }
            else if ( !pressed && (attackState == AttackState.AttackReady) )
            {
                if ( m_AttackCoroutin != null )
                {
                    StopCoroutine(m_AttackCoroutin);
                    m_AttackCoroutin = null;
                }

                if ( m_AttackMoveCoroutin != null )
                {
                    StopCoroutine(m_AttackMoveCoroutin);
                    m_AttackMove = Vector2.zero;
                }

                m_Animator.SetBool("Attack", false);

                m_IsAttackReady = false;

                m_Sword_VR.SetActive(false);
                m_Trail_VR.Use = false;

                m_Sword_Desktop.SetActive(false);

                m_GuideController.Disappear();

                SetEnableLocomotion(true);
                
                foreach ( var i in m_EnableObjectsOnAttack )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableObjectsOnAttack )
                {
                    i.SetActive(true);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = true;
                }

                m_SwordTriggerDesktop.ResetAttackTime();
            }

            var src = m_Animator.GetFloat("AttackReady");
            var dest = m_IsAttackReady ? 1f : 0f;

            var attackReady = Mathf.Lerp(src, dest, t);

            if ( Mathf.Abs(attackReady - dest) < 0.001f )
            {
                attackReady = dest;
            }

            m_Animator.SetFloat("AttackReady", attackReady);

            var destOffset = 0f;

            if ( m_IsAttackReady )
            {
                destOffset = m_AttackCameraOffset.localPosition.y + m_CharacterCenterEye.position.y - m_ForwardSource.position.y;
            }

            var y = Mathf.Lerp(m_AttackCameraOffset.localPosition.y, destOffset, t);

            if ( Mathf.Abs(y - destOffset) < 0.001f )
            {
                y = destOffset;
            }

            m_AttackCameraOffset.localPosition = new Vector3(0f, y, 0f);
        }

        private void AttackGuide( int attackIndex )
        {
            var angle = 0f;

            switch ( attackIndex )
            {
                case 0:
                    angle = 180f + 30f;
                    break;

                case 1:
                    angle = 20f;
                    break;

                case 2:
                    angle = 0f;
                    break;

                default:
                    break;
            }

            m_GuideController.ShowUp(angle, () =>
            {
                Attack(attackIndex);

                if ( m_AttackCoroutin != null )
                {
                    StopCoroutine(m_AttackCoroutin);
                }

                m_AttackCoroutin = StartCoroutine(
                    Callback(() => 
                    {
                        m_AttackCoroutin = null;

                        AttackGuide((attackIndex + 1) % 3);
                    },
                    0.5f
                ));
            });

            m_LastAttackIndex = attackIndex;
        }

        private bool UpdateOffsetSetup()
        {
            var attackState = GetAttackState();
 
            var input1 = m_RightHandPrimaryButtonAction.action?.ReadValue<float>() ?? 0f;
            var input2 = m_RightHandSecondaryButtonAction.action?.ReadValue<float>() ?? 0f;
            var pressed = (input1 > 0.75f) || (input2 > 0.75f);

            if ( pressed && !m_IsOffsetSetup && (attackState == AttackState.Idle) )
            {
                m_IsOffsetSetup = true;

                SetEnableLocomotion(false);

                m_Sword_VR.SetActive(true);
                
                foreach ( var i in m_EnableObjectsOnOffsetSetup )
                {
                    i.SetActive(true);
                }
                
                foreach ( var i in m_DisableObjectsOnOffsetSetup )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = false;
                }

                m_LeftHandTransform.parent = null;
                m_RightHandTransform.parent = null;
            }
            else if ( !pressed && m_IsOffsetSetup )
            {
                m_IsOffsetSetup = false;

                SetEnableLocomotion(true);
                
                m_Sword_VR.SetActive(false);
                
                foreach ( var i in m_EnableObjectsOnOffsetSetup )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableObjectsOnOffsetSetup )
                {
                    i.SetActive(true);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = true;
                }

                m_LeftHandTransform.parent = m_OffsetLeftParent;
                m_RightHandTransform.parent = m_OffsetRightParent;

                var position = m_RightHandTransform.localPosition;
                position.x *= -1f;

                m_LeftHandTransform.localPosition = position;
            }

            return m_IsOffsetSetup;
        }

        private void UpdateGuideAB()
        {
            var attackState = GetAttackState();

            var input = m_LeftHandSelectAction.action?.ReadValue<float>() ?? 0f;
            var pressed = (input > 0.75f);

            if ( pressed && !m_GuideABPressed && (attackState == AttackState.Idle) )
            {
                m_GuideABPressed = true;

                SwapGuideAB();
            }
            else if ( !pressed && m_GuideABPressed )
            {
                m_GuideABPressed = false;
            }
        }

        private void SwapGuideAB()
        {
            m_SelectAudio.Stop();
            m_SelectAudio.Play();

            switch ( m_GuideABIndex )
            {
                case 0:
                    m_GuideABIndex = 1;

                    m_GuideA.SetActive(false);

                    m_GuideB.SetActive(true);
                    m_GuideB.GetComponent<AlphaFade>().Initialize();

                    break;

                case 1:
                    m_GuideABIndex = 0;

                    m_GuideB.SetActive(false);

                    m_GuideA.SetActive(true);
                    m_GuideA.GetComponent<AlphaFade>().Initialize();

                    break;

                default:
                    break;
            }
        }

        private void UpdateAttackA( float duration )
        {
            if ( m_GuideABIndex != 0 )
            {
                return;
            }

            var t = Mathf.Clamp01(Time.deltaTime / duration);

            var attackState = GetAttackState();

            var input = m_RightHandSelectAction.action?.ReadValue<float>() ?? 0f;
            var pressed = (input > 0.75f);

            if ( pressed && (attackState == AttackState.Idle) )
            {
                m_AttackA = true;
                m_IsAttackReady = true;

                SetEnableLocomotion(false);

                m_AttackASword.SetActive(true);
                m_Trail_VR_A.Use = true;

                foreach ( var i in m_EnableObjectsOnAttack )
                {
                    i.SetActive(true);
                }
                
                foreach ( var i in m_DisableObjectsOnAttack )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = false;
                }
            }
            else if ( !pressed && (attackState == AttackState.AttackReady) )
            {
                m_AttackA = false;
                m_IsAttackReady = false;

                SetEnableLocomotion(true);

                m_AttackASword.SetActive(false);
                m_Trail_VR_A.Use = false;

                foreach ( var i in m_EnableObjectsOnAttack )
                {
                    i.SetActive(false);
                }

                foreach ( var i in m_DisableObjectsOnAttack )
                {
                    i.SetActive(true);
                }

                foreach ( var i in m_DisableXRRayInteratorOnAttack )
                {
                    i.enabled = true;
                }
            }

            var src = m_Animator.GetFloat("AttackReady");
            var dest = m_IsAttackReady ? 1f : 0f;

            var attackReady = Mathf.Lerp(src, dest, t);

            if ( Mathf.Abs(attackReady - dest) < 0.001f )
            {
                attackReady = dest;
            }

            m_Animator.SetFloat("AttackReady", attackReady);

            var destOffset = 0f;

            if ( m_IsAttackReady )
            {
                destOffset = m_AttackCameraOffset.localPosition.y + m_CharacterCenterEye.position.y - m_ForwardSource.position.y;
            }

            var y = Mathf.Lerp(m_AttackCameraOffset.localPosition.y, destOffset, t);

            if ( Mathf.Abs(y - destOffset) < 0.001f )
            {
                y = destOffset;
            }

            m_AttackCameraOffset.localPosition = new Vector3(0f, y, 0f);
        }
        
        public void UpdateAnimatorIK( int layerIndex )
        {
            var value = m_AttackA ? 1f : 0f;

            m_Animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, value);
            m_Animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, value);
            m_Animator.SetIKPosition(AvatarIKGoal.LeftHand, m_LeftHandIKTransform.position);
            m_Animator.SetIKRotation(AvatarIKGoal.LeftHand, m_LeftHandIKTransform.rotation);

            m_Animator.SetIKPositionWeight(AvatarIKGoal.RightHand, value);
            m_Animator.SetIKRotationWeight(AvatarIKGoal.RightHand, value);
            m_Animator.SetIKPosition(AvatarIKGoal.RightHand, m_RightHandIKTransform.position);
            m_Animator.SetIKRotation(AvatarIKGoal.RightHand, m_RightHandIKTransform.rotation);
        }
    }
}