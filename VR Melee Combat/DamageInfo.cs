using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageInfo : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target;

    private void Update()
    {
        var position = transform.position;
        position.y += 0.5f * Time.deltaTime;

        transform.position = position;

        transform.LookAt(m_Target);
    }
}
