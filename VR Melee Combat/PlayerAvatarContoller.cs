using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerAvatarContoller : MonoBehaviour
{
    [SerializeField]
    private CharacterController m_CharacterController;

    [SerializeField]
    private CustomLocomotionProvider m_CustomLocomotionProvider;

    private void Update()
    {
        var position = m_CharacterController.transform.position;
        position += (m_CharacterController.transform.right * m_CharacterController.center.x);
        position += (m_CharacterController.transform.forward * m_CharacterController.center.z);

        var rotation = transform.rotation.eulerAngles;
        rotation.y = Camera.main.transform.rotation.eulerAngles.y;

        transform.SetPositionAndRotation(position, Quaternion.Euler(rotation));
    }

    private void OnAnimatorIK( int layerIndex )
    {
        m_CustomLocomotionProvider.UpdateAnimatorIK(layerIndex);
    }
}
