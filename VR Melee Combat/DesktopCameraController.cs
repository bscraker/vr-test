using UnityEngine;

public class DesktopCameraController : MonoBehaviour
{
    private readonly float mouseSensitivity = 100.0f;
    private readonly float clampAngle = 90.0f;

    private float rotY = 0.0f;
    private float rotX = 0.0f;

    private void Awake()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

    private void Update()
    {
        UpdateRotation();
        UpdatePosition();

        Camera.allCameras[2].fieldOfView = Camera.allCameras[0].fieldOfView;

        if ( Input.GetKeyDown(KeyCode.Tab) )
        {
            (Camera.allCameras[1].depth, Camera.allCameras[2].depth) =
                (Camera.allCameras[2].depth, Camera.allCameras[1].depth);
        }
    }

    private void UpdatePosition()
    {
        float xAxisValue = 0f;
        float yAxisValue = 0f;
        float zAxisValue = 0f;

        if ( Input.GetKey(KeyCode.D) )
        {
            xAxisValue += 1f;
        }
        else if ( Input.GetKey(KeyCode.A) )
        {
            xAxisValue -= 1f;
        }

        if ( Input.GetKey(KeyCode.E) )
        {
            yAxisValue += 1f;
        }
        else if ( Input.GetKey(KeyCode.Q) )
        {
            yAxisValue -= 1f;
        }

        if ( Input.GetKey(KeyCode.W) )
        {
            zAxisValue += 1f;
        }
        else if ( Input.GetKey(KeyCode.S) )
        {
            zAxisValue -= 1f;
        }

        var speed = 1f;

        if ( Input.GetKey(KeyCode.LeftShift) )
        {
            speed *= 2f;
        }

        transform.Translate(speed * Time.deltaTime * new Vector3(xAxisValue, yAxisValue, zAxisValue));
    }

    private void UpdateRotation()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }
}
