using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using RenderPipeline = UnityEngine.Rendering.RenderPipelineManager;

[ExecuteInEditMode]
public class Vignetting : MonoBehaviour
{
    [Range(0f, 1f)]
    public float m_CutoutSize = 0.3f;

#if UNITY_EDITOR
    private GameObject m_XRDeviceSimulator;

    private void Awake()
    {
        m_XRDeviceSimulator = GameObject.Find("XR Device Simulator");
    }
#endif

    private void OnEnable()
    {
        RenderPipeline.beginCameraRendering += UpdateCamera;
    }

    private void OnDisable()
    {
        RenderPipeline.beginCameraRendering -= UpdateCamera;
    }

    void UpdateCamera( ScriptableRenderContext SRC, Camera camera )
    {
        var target = Camera.main.transform.position;
        target += Camera.main.transform.forward * 0.5f;

        var leftCutoutPosition = Camera.main.WorldToViewportPoint(target, Camera.MonoOrStereoscopicEye.Left);
        leftCutoutPosition.y /= (Screen.width / Screen.height);
    
        var rightCutoutPosition = Camera.main.WorldToViewportPoint(target, Camera.MonoOrStereoscopicEye.Right);
        rightCutoutPosition.y /= (Screen.width / Screen.height);

        Shader.SetGlobalVector("_Left_Cutout_Position", leftCutoutPosition);
        Shader.SetGlobalVector("_Right_Cutout_Position", rightCutoutPosition);
        Shader.SetGlobalFloat("_Cutout_Size", m_CutoutSize);
        
#if UNITY_EDITOR
        if ( m_XRDeviceSimulator && m_XRDeviceSimulator.activeSelf )
        {
            leftCutoutPosition = Camera.main.WorldToViewportPoint(target);
            leftCutoutPosition.y /= (Screen.width / Screen.height);
            Shader.SetGlobalVector("_Left_Cutout_Position", leftCutoutPosition);
        }
#endif

        UniversalRenderPipeline.RenderSingleCamera(SRC, camera);
    }
}
