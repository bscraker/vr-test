using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaFade : MonoBehaviour
{
    public FadeType m_FadeType;
    
    public bool m_StartOnAwake = true;

    public float m_Delay = 0f;
    public float m_Time = 0.25f;

    private float m_Tick;
    private float m_AlphaStart;
    private float m_AlphaEnd;

    private bool m_Finish = false;

    public enum FadeType
    {
        In,
        Out
    }

    private void Awake()
    {
        if ( m_StartOnAwake )
        {
            Initialize();
        }
    }

    private void Update()
    {
        if ( m_Finish )
        {
            return;
        }

        var t = Mathf.Clamp01((Time.time - m_Tick) / m_Time);

        Recursive(transform, (meshRenderer) =>
        {
            var color = meshRenderer.material.color;
            color.a = Mathf.Lerp(m_AlphaStart, m_AlphaEnd, t);

            meshRenderer.material.color = color;
        });

        if ( t >= 1f )
        {
            m_Finish = true;
        }
    }

    public void Initialize()
    {
        m_Tick = Time.time + m_Delay;
        
        switch ( m_FadeType )
        {
            case FadeType.In:
                m_AlphaEnd = 1f;
                break;

            case FadeType.Out:
                m_AlphaEnd = 0f;
                break;

            default:
                break;
        }

        m_AlphaStart = 1f - m_AlphaEnd;
        
        Recursive(transform, (meshRenderer) =>
        {
            var color = meshRenderer.material.color;
            color.a = m_AlphaStart;

            meshRenderer.material.color = color;
        });

        m_Finish = false;
    }

    private void Recursive( Transform parent, Action<MeshRenderer> callback )
    {
        for ( var i = 0; i < parent.childCount; ++i )
        {
            var child = parent.GetChild(i);
            
            if ( child.TryGetComponent<MeshRenderer>(out var meshRenderer) )
            {
                callback(meshRenderer);
            }
            else
            {
                Recursive(child, callback);
            }
        }
    }
}
