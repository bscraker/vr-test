using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeoutDestroy : MonoBehaviour
{
    public float m_Delay = 0.25f;
    
    public bool m_StartOnAwake = true;

    public GameObject m_InstantiateObject = null;

    private bool m_Started = false;

    private void Awake()
    {
        if ( m_StartOnAwake )
        {
            StartCoroutine(Timeout());
        }
    }

    private IEnumerator Timeout()
    {
        if ( m_Started )
        {
            yield break;
        }

        m_Started = true;

        yield return new WaitForSeconds(m_Delay);

        if ( m_InstantiateObject != null )
        {
            Instantiate(m_InstantiateObject);
        }

        Destroy(gameObject);
    }

    public void StartTimeout()
    {
        StartCoroutine(Timeout());
    }
}
