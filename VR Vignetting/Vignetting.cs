using UniRx;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Vignetting : MonoBehaviour
{
#if UNITY_EDITOR
    private static Camera.MonoOrStereoscopicEye m_Eye = Camera.MonoOrStereoscopicEye.Left;
#else
    private static readonly Camera.MonoOrStereoscopicEye m_Eye = Camera.MonoOrStereoscopicEye.Left;
#endif

    private static readonly int m_PropertyEyeCenterOffset = Shader.PropertyToID("_EyeCenterOffset");
    private static readonly int m_PropertyRadiusRatio = Shader.PropertyToID("_RadiusRatio");
    private static readonly int m_PropertyApertureSize = Shader.PropertyToID("_ApertureSize");
    private static readonly int m_PropertyFeatheringEffect = Shader.PropertyToID("_FeatheringEffect");

    private static readonly float m_FadeTime = 0.25f;

    public bool m_AlwaysEnabled;

    [Range(0f, 1f)]
    public float m_ApertureSize = 0.7f;
    private readonly FloatReactiveProperty m_AppliedApertureSize = new();
    
    [RangeReactiveProperty(0f, 1f)]
    public FloatReactiveProperty m_FeatheringEffect = new(0.2f);

    [SerializeField]
    private Camera _MainCamera;
    private static Camera m_MainCamera;

    [SerializeField]
    private Camera m_VignettingCamera;

    [SerializeField]
    private LocomotionProvider[] m_LocomotionProviders = new LocomotionProvider[0];

    private static Vector3 m_PositionOffset;
    private static Vector3 m_RotationOffset;

    private static Transform m_MainCameraTransform;
    private static Transform m_VignettingCameraTransform;

    private float m_LocomotionTime;

    private void Awake()
    {
        m_MainCamera = _MainCamera;
        m_MainCameraTransform = m_MainCamera.transform;
        m_VignettingCameraTransform = m_VignettingCamera.transform;

        m_PositionOffset =
            m_VignettingCameraTransform.localPosition
            - m_MainCameraTransform.localPosition;
        m_RotationOffset =
            m_VignettingCameraTransform.localRotation.eulerAngles
            - m_MainCameraTransform.localRotation.eulerAngles;

#if UNITY_EDITOR
        if ( !m_MainCamera.stereoEnabled )
        {
            m_Eye = Camera.MonoOrStereoscopicEye.Mono;
        }
#endif

        m_MainCameraTransform
            .ObserveEveryValueChanged(x =>
            {
                var target = x.position + (x.forward * 0.5f);

                var center = m_MainCamera.WorldToViewportPoint(target, m_Eye);

                //(0.50, 1.06, -0.50)
                //(0.50, 1.06, -0.50)
                //(0.54, 0.50, 0.50)
                //(0.46, 0.50, 0.50)

                return center;
            }, FrameCountType.EndOfFrame).Subscribe(x =>
            {
                var eyeOffset = x.x - 0.5f;

                Shader.SetGlobalFloat(m_PropertyEyeCenterOffset, eyeOffset);
            });

        m_MainCameraTransform
            .ObserveEveryValueChanged(x =>
            {
                var target = m_MainCamera.ViewportToWorldPoint(Vector3.forward, m_Eye);
                target += (x.right + x.up) * 0.5f;

                var vertex = m_MainCamera.WorldToViewportPoint(target, m_Eye);

                return vertex;
            }, FrameCountType.EndOfFrame).Subscribe(x =>
            {
                x.x = 0.5f / x.x;
                x.y = 0.5f / x.y;

                Shader.SetGlobalVector(m_PropertyRadiusRatio, x);
            });

        m_AppliedApertureSize
            .DistinctUntilChanged()
            .Subscribe(x => Shader.SetGlobalFloat(m_PropertyApertureSize, (x * x)));

        m_FeatheringEffect
            .DistinctUntilChanged()
            .Subscribe(x => Shader.SetGlobalFloat(m_PropertyFeatheringEffect, 0.5f / (x * x)));

        m_MainCameraTransform
            .ObserveEveryValueChanged(x => (x.localPosition, x.localRotation))
            .Subscribe(x =>
            {
                m_VignettingCameraTransform.SetLocalPositionAndRotation(
                    x.localPosition + m_PositionOffset,
                    Quaternion.Euler(x.localRotation.eulerAngles + m_RotationOffset)
                );
            });

        m_LocomotionTime = -m_FadeTime * 2f;
    }

    private void Update()
    {
        UpdateApertureSize();
    }

    private void UpdateApertureSize()
    {
        if ( m_AlwaysEnabled )
        {
            m_AppliedApertureSize.Value = m_ApertureSize;

            return;
        }

        var length = m_LocomotionProviders.Length;

        for ( var i = 0; i < length; ++i )
        {
            switch ( m_LocomotionProviders[i].locomotionPhase )
            {
                case LocomotionPhase.Started:
                case LocomotionPhase.Moving:
                    m_AppliedApertureSize.Value = m_ApertureSize;
                    m_LocomotionTime = Time.time;
                    return;

                default:
                    break;
            }
        }

        var t = (Time.time - m_LocomotionTime) / m_FadeTime;
        t = Mathf.Min(t, 2f);

        m_AppliedApertureSize.Value
            = Mathf.LerpUnclamped(m_ApertureSize, 1f, t);
    }
}
