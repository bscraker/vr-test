using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Renderer))]
public class MaterialPropertyInitializer : MonoBehaviour
{
    private class ReadOnlyFieldAttribute : PropertyAttribute {}

    [CustomPropertyDrawer(typeof(ReadOnlyFieldAttribute))]
    private class ReadOnlyFieldDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }

    [Serializable]
    private struct MaterialProperty
    {
        public string name;
        [ReadOnlyField] public int id;
        public ShaderPropertyType type;
        public bool enable;
        public Color colorValue;
        public float floatValue;
        public Vector4 vectorValue;
    }

    [SerializeField]
    private MaterialProperty[] m_MaterialProperties = new MaterialProperty[0];

    private Renderer m_Renderer;
    private Material m_Material;
    private Shader m_Shader;

    private MaterialPropertyBlock m_MaterialPropertyBlock;

#if UNITY_EDITOR
    private bool m_Add = false;
#endif

    private void Awake()
    {
        Initialize();

#if UNITY_EDITOR
        if ( m_MaterialProperties.Length == 0 || m_Add )
        {
            AddMissingProperties();

            m_Add = false;
        }
#endif

#if UNITY_EDITOR
        GetPropertyValues();
#endif

        SetPropertyValues();

        m_Renderer.SetPropertyBlock(m_MaterialPropertyBlock);
    }

    private void Initialize()
    {
        m_Renderer = GetComponent<Renderer>();
        m_Material = m_Renderer.sharedMaterial;
        m_Shader = m_Material.shader;

        m_MaterialPropertyBlock = new();
    }

    private void SetPropertyValues()
    {
        var count = m_MaterialProperties.Length;

        for ( int i = 0; i < count; ++i )
        {
#if UNITY_EDITOR
            m_MaterialProperties[i].id = Shader.PropertyToID(m_MaterialProperties[i].name);

            if ( !m_Material.HasProperty(m_MaterialProperties[i].id) )
            {
                m_MaterialProperties[i].id = -1;
                continue;
            }
#endif

            if ( !m_MaterialProperties[i].enable )
            {
                continue;
            }

            switch ( m_MaterialProperties[i].type )
            {
                case ShaderPropertyType.Color:
                    m_MaterialPropertyBlock.SetColor(m_MaterialProperties[i].id, m_MaterialProperties[i].colorValue);
                    break;

                case ShaderPropertyType.Float:
                case ShaderPropertyType.Range:
                    m_MaterialPropertyBlock.SetFloat(m_MaterialProperties[i].id, m_MaterialProperties[i].floatValue);
                    break;

                case ShaderPropertyType.Vector:
                    m_MaterialPropertyBlock.SetVector(m_MaterialProperties[i].id, m_MaterialProperties[i].vectorValue);
                    break;

                default:
                    break;
            }
        }
    }

#if UNITY_EDITOR
    private void GetPropertyValues()
    {
        var count = m_Shader.GetPropertyCount();

        for ( int i = 0; i < count; ++i )
        {
            var length = m_MaterialProperties.Length;
            
            for ( int j = 0; j < length; ++j )
            {
                if ( m_MaterialProperties[j].name != m_Shader.GetPropertyName(i) )
                {
                    continue;
                }

                if ( m_Shader.GetPropertyType(i) == ShaderPropertyType.Range )
                {
                    var range = m_Shader.GetPropertyRangeLimits(i);

                    m_MaterialProperties[j].floatValue = Mathf.Clamp(m_MaterialProperties[j].floatValue, range.x, range.y);
                }

                if ( !m_MaterialProperties[j].enable )
                {
                    m_MaterialProperties[j].id = Shader.PropertyToID(m_MaterialProperties[j].name);

                    if ( !m_Material.HasProperty(m_MaterialProperties[j].id) )
                    {
                        m_MaterialProperties[j].id = -1;
                        continue;
                    }

                    //switch ( m_MaterialProperties[j].type )
                    //{
                    //    case ShaderPropertyType.Color:
                    //        m_MaterialPropertyBlock.SetColor(m_MaterialProperties[j].id, m_Shader.GetPropertyDefaultVectorValue(i));
                    //        break;

                    //    case ShaderPropertyType.Float:
                    //    case ShaderPropertyType.Range:
                    //        m_MaterialPropertyBlock.SetFloat(m_MaterialProperties[j].id, m_Shader.GetPropertyDefaultFloatValue(i));
                    //        break;

                    //    case ShaderPropertyType.Vector:
                    //        m_MaterialPropertyBlock.SetVector(m_MaterialProperties[j].id, m_Shader.GetPropertyDefaultVectorValue(i));
                    //        break;

                    //    default:
                    //        break;
                    //}
                }

                break;
            }
        }
    }

    private void AddMissingProperties()
    {
        var count = m_Shader.GetPropertyCount();

        for ( int i = 0; i < count; ++i )
        {
            if ( m_Shader.GetPropertyFlags(i).HasFlag(ShaderPropertyFlags.HideInInspector) )
            {
                continue;
            }

            var name = m_Shader.GetPropertyName(i);

            var check = true;

            foreach ( var item in m_MaterialProperties )
            {
                if ( item.name.Equals(name) )
                {
                    check = false;
                    break;
                }
            }

            if ( !check )
            {
                continue;
            }

            Array.Resize(ref m_MaterialProperties, m_MaterialProperties.Length + 1);

            m_MaterialProperties[^1].name = name;
            m_MaterialProperties[^1].type = m_Shader.GetPropertyType(i);

            switch ( m_MaterialProperties[^1].type )
            {
                case ShaderPropertyType.Color:
                    m_MaterialProperties[^1].colorValue = m_Shader.GetPropertyDefaultVectorValue(i);
                    break;

                case ShaderPropertyType.Float:
                case ShaderPropertyType.Range:
                    m_MaterialProperties[^1].floatValue = m_Shader.GetPropertyDefaultFloatValue(i);
                    break;

                case ShaderPropertyType.Vector:
                    m_MaterialProperties[^1].vectorValue = m_Shader.GetPropertyDefaultVectorValue(i);
                    break;

                default:
                    break;
            }
        }
    }

    private void OnValidate()
    {
        Awake();
    }

    [ContextMenu("Add missing properties")]
    private void AddMenu()
    {
        m_Add = true;

        Awake();
    }
#endif

    public MaterialPropertyBlock GetPropertyBlock()
    {
        if ( m_MaterialPropertyBlock == null )
        {
            Initialize();
            SetPropertyValues();
        }

        return m_MaterialPropertyBlock;
    }
}
