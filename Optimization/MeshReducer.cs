#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityMeshSimplifier;
#endif

[RequireComponent(typeof(MeshFilter))]
public class MeshReducer : MonoBehaviour
{
    private class ReadOnlyFieldAttribute : PropertyAttribute {}

    [CustomPropertyDrawer(typeof(ReadOnlyFieldAttribute))]
    private class ReadOnlyFieldDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }

    [CustomEditor(typeof(MeshReducer))]
    private class CubeGenerateButton : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if ( GUILayout.Button("Reduce") && Event.current.button == 0 )
            {
                ((MeshReducer)target).Reduce();
            }
        }
    }

    [Header("Configure")]
    [SerializeField, Range(0f, 1f)]
    private float m_Quality = 0.5f;

    [SerializeField, ReadOnlyField]
    private int m_OriginalTris;

    [SerializeField, ReadOnlyField]
    private int m_CalculatedTris;

    [SerializeField]
    private bool m_RemoveUvs = true;

    [SerializeField]
    private bool m_RemoveColors = true;

    [SerializeField]
    private bool m_RemoveBones = true;

    [SerializeField]
    private bool m_ReadWriteEnabled = false;

    private void OnValidate()
    {
        var originalMesh = GetComponent<MeshFilter>().sharedMesh;
        var tris = originalMesh.triangles;
        
        m_OriginalTris = Mathf.RoundToInt(tris.Length / 3f);
        m_CalculatedTris = (int)(m_OriginalTris * m_Quality);
    }

    private void Reduce()
    {
        var originalMesh = GetComponent<MeshFilter>().sharedMesh;

        var meshSimplifier = new MeshSimplifier();
        meshSimplifier.Initialize(originalMesh);

        var options = meshSimplifier.SimplificationOptions;
        //options.PreserveBorderEdges = true;
        //options.PreserveUVSeamEdges = true;
        //options.PreserveUVFoldoverEdges = true;
        //options.PreserveSurfaceCurvature = true;
        options.MaxIterationCount = 1000;
        options.Agressiveness = 100f;

        meshSimplifier.SimplificationOptions = options;
        meshSimplifier.SimplifyMesh(m_Quality);
        
        var destMesh = meshSimplifier.ToMesh();

        if ( m_RemoveUvs )
        {
            destMesh.uv = null;
            destMesh.uv2 = null;
            destMesh.uv3 = null;
            destMesh.uv4 = null;
            destMesh.uv5 = null;
            destMesh.uv6 = null;
            destMesh.uv7 = null;
            destMesh.uv8 = null;
        }

        if ( m_RemoveColors )
        {
            destMesh.colors = null;
            destMesh.colors32 = null;
        }

        if ( m_RemoveBones )
        {
            destMesh.bindposes = null;
            destMesh.boneWeights = null;
        }

        destMesh.UploadMeshData(!m_ReadWriteEnabled);

        var folderName = "ReducedMesh";
        var folderPath = Path.Combine("Assets", folderName);
        if (!AssetDatabase.IsValidFolder(folderPath))
            AssetDatabase.CreateFolder("Assets", folderName);

        AssetDatabase.CreateAsset(destMesh, Path.Combine(folderPath, name + ".asset"));
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Debug.Log("Reduce Done");
    }
}
