﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Deform;

using UnityEditor;
using System.IO;
#endif

public class AnimationClipTextureBaker : MonoBehaviour
{
    private class ReadOnlyFieldAttribute : PropertyAttribute {}

    [CustomPropertyDrawer(typeof(ReadOnlyFieldAttribute))]
    private class ReadOnlyFieldDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }

    [CustomEditor(typeof(AnimationClipTextureBaker))]
    private class CubeGenerateButton : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if ( GUILayout.Button("Bake") && Event.current.button == 0 )
            {
                ((AnimationClipTextureBaker)target).Bake();
            }
        }
    }

    [SerializeField]
    private ComputeShader infoTexGen;

    [SerializeField]
    private Shader playShader;

    [Header("Configure")]
    [SerializeField]
    private int targetFrame = 32;

    [SerializeField, ReadOnlyField]
    private float calculatedFPS;

    [SerializeField, ReadOnlyField]
    private float calculatedFileSizeInKB;

    [Header("Only the first clip will be baked")]
    [SerializeField]
    private AnimationClip[] clips;

    public struct VertInfo
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector3 tangent;
    }

    private void Reset()
    {
        var animation = GetComponent<Animation>();
        var animator = GetComponent<Animator>();

        if (animation != null)
        {
            clips = new AnimationClip[animation.GetClipCount()];
            var i = 0;
            foreach (AnimationState state in animation)
                clips[i++] = state.clip;
        }
        else if (animator != null)
            clips = animator.runtimeAnimatorController.animationClips;
    }

    private void OnValidate()
    {
        var skin = GetComponentInChildren<SkinnedMeshRenderer>();

        var meshFilter = GetComponent<MeshFilter>();
        var deformable = GetComponent<Deformable>();

        Mesh sharedMesh;

        if ( skin != null )
        {
            sharedMesh = skin.sharedMesh;

            if ( sharedMesh == null )
            {
                return;
            }
        }
        else if ( meshFilter != null && deformable != null )
        {
            sharedMesh = deformable.GetOriginalMesh();
        }
        else
        {
            return;
        }

        if ( clips.Length == 0 )
        {
            calculatedFileSizeInKB = -1f;
            calculatedFPS = -1f;

            return;
        }

        var vCount = sharedMesh.vertexCount;
        var clipLength = clips[0].length;

        if ( targetFrame < 1 )
        {
            targetFrame = 1;
        }
        else if ( (targetFrame / clipLength) > 60f )
        {
            targetFrame = (int)(60f * clipLength);
        }

        calculatedFileSizeInKB = vCount * targetFrame * 8f / 1024f * 3f;
        calculatedFPS = targetFrame / clipLength;
    }

    //[ContextMenu("Bake Animation Texture")]
    void Bake()
    {
        if ( infoTexGen == null )
        {
            Debug.LogWarning("[InfoTexGen] is empty in inspector");
            return;
        }
        
        if ( playShader == null )
        {
            Debug.LogWarning("[PlayShader] is empty in inspector");
            return;
        }

        var skin = GetComponentInChildren<SkinnedMeshRenderer>();

        var meshFilter = GetComponent<MeshFilter>();
        var deformable = GetComponent<Deformable>();

        Mesh sharedMesh;
        Mesh bakedMesh;

        if ( skin != null )
        {
            sharedMesh = skin.sharedMesh;
            bakedMesh = new Mesh();
        }
        else if ( meshFilter != null && deformable != null )
        {
            sharedMesh = deformable.GetOriginalMesh();
            bakedMesh = deformable.GetCurrentMesh();
        }
        else
        {
            Debug.LogError("Mesh not found");
            return;
        }

        //var vCount = skin.sharedMesh.vertexCount;
        var vCount = sharedMesh.vertexCount;
        //var texWidth = Mathf.NextPowerOfTwo(vCount);
        var texWidth = vCount;

        foreach (var clip in clips)
        {
            //var frames = Mathf.NextPowerOfTwo((int)(clip.length / 0.05f));
            //var frames = Mathf.NextPowerOfTwo((int)(clip.length / (1f / targetFPS)));
            //var frames = Mathf.NextPowerOfTwo(targetFrame);
            var frames = targetFrame;
            var dt = clip.length / frames;
            var infoList = new List<VertInfo>();

            var pRt = new RenderTexture(texWidth, frames, 0, RenderTextureFormat.ARGBHalf);
            pRt.name = string.Format("{0}.{1}.posTex", name, clip.name);
            var nRt = new RenderTexture(texWidth, frames, 0, RenderTextureFormat.ARGBHalf);
            nRt.name = string.Format("{0}.{1}.normTex", name, clip.name);
            var tRt = new RenderTexture(texWidth, frames, 0, RenderTextureFormat.ARGBHalf);
            tRt.name = string.Format("{0}.{1}.tanTex", name, clip.name);

            foreach (var rt in new[] { pRt, nRt, tRt })
            {
                rt.enableRandomWrite = true;
                rt.Create();
                RenderTexture.active = rt;
                GL.Clear(true, true, Color.clear);
            }

            for (var i = 0; i < frames; i++)
            {
                clip.SampleAnimation(gameObject, dt * i);
                
                if ( skin != null )
                {
                    skin.BakeMesh(bakedMesh);
                }
                else if ( meshFilter != null && deformable != null )
                {
                    deformable.PreSchedule();
                    deformable.Schedule();
                    deformable.Complete();
                    deformable.ApplyData(true);
                }

                infoList.AddRange(Enumerable.Range(0, vCount)
                    .Select(idx => new VertInfo()
                    {
                        position = bakedMesh.vertices[idx],
                        normal = bakedMesh.normals[idx],
                        tangent = bakedMesh.tangents[idx],
                    })
                );
            }

            var buffer = new ComputeBuffer(infoList.Count, System.Runtime.InteropServices.Marshal.SizeOf(typeof(VertInfo)));
            buffer.SetData(infoList.ToArray());

            var kernel = infoTexGen.FindKernel("CSMain");
            uint x, y, z;
            infoTexGen.GetKernelThreadGroupSizes(kernel, out x, out y, out z);

            infoTexGen.SetInt("VertCount", vCount);
            infoTexGen.SetBuffer(kernel, "Info", buffer);
            infoTexGen.SetTexture(kernel, "OutPosition", pRt);
            infoTexGen.SetTexture(kernel, "OutNormal", nRt);
            infoTexGen.SetTexture(kernel, "OutTangent", tRt);
            infoTexGen.Dispatch(kernel, vCount / (int)x + 1, frames / (int)y + 1, 1);

            buffer.Release();

#if UNITY_EDITOR
            var folderName = "BakedAnimationTex";
            var folderPath = Path.Combine("Assets", folderName);
            if (!AssetDatabase.IsValidFolder(folderPath))
                AssetDatabase.CreateFolder("Assets", folderName);

            var subFolder = name;
            var subFolderPath = Path.Combine(folderPath, subFolder);
            if (!AssetDatabase.IsValidFolder(subFolderPath))
                AssetDatabase.CreateFolder(folderPath, subFolder);

            var posTex = RenderTextureToTexture2D.Convert(pRt);
            var normTex = RenderTextureToTexture2D.Convert(nRt);
            var tanTex = RenderTextureToTexture2D.Convert(tRt);
            //Graphics.CopyTexture(pRt, posTex);
            //Graphics.CopyTexture(nRt, normTex);
            //Graphics.CopyTexture(tRt, tanTex);

            var mat = new Material(playShader);
            //mat.SetTexture("_MainTex", skin.sharedMaterial.mainTexture);
            //mat.SetTexture("_PosTex", posTex);
            //mat.SetTexture("_NmlTex", normTex);
            //mat.SetFloat("_Length", clip.length);
            //if (clip.wrapMode == WrapMode.Loop)
            //{
            //    mat.SetFloat("_Loop", 1f);
            //    mat.EnableKeyword("ANIM_LOOP");
            //}
            mat.SetTexture("_PositionTexture", posTex);
            mat.SetTexture("_NormalTexture", normTex);
            mat.SetTexture("_TangentTexture", tanTex);
            mat.SetFloat("_TextureWidth", texWidth);
            mat.SetFloat("_AnimationLength", clip.length);
            mat.SetVector("_BakedScale", transform.lossyScale);
            mat.enableInstancing = true;

            var go = new GameObject(name + "." + clip.name);
            go.AddComponent<MeshFilter>().sharedMesh = sharedMesh;
            go.AddComponent<MeshRenderer>().sharedMaterial = mat;
            go.transform.localScale = transform.lossyScale;

            AssetDatabase.CreateAsset(posTex, Path.Combine(subFolderPath, pRt.name + ".asset"));
            AssetDatabase.CreateAsset(normTex, Path.Combine(subFolderPath, nRt.name + ".asset"));
            AssetDatabase.CreateAsset(tanTex, Path.Combine(subFolderPath, tRt.name + ".asset"));
            AssetDatabase.CreateAsset(mat, Path.Combine(subFolderPath, string.Format("{0}.{1}.animTex.asset", name, clip.name)));
            PrefabUtility.SaveAsPrefabAsset(go, Path.Combine(subFolderPath, go.name + ".prefab").Replace("\\", "/"));
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Debug.Log("Bake Done");
#endif
            break;
        }
    }
}
